---
title: pth文件的编码
date: 2017-05-27 12:50:48
tags: python 编码
---
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-8eee213734fdca7906f567416c97b40b_r.jpg?raw=true">
### 前注
之前不知从啥时候开始，用py3一直读取不了文件，但是用py2可以顺利读取。自己试了好多方法都不行（包括重装），最近要写一个东西，莫名其妙地又好了。今天做个笔记以备后用，外加终结玄学】
### 文件操作模板
1. 内建函数open，操作完需手动关闭  
`fileObj=open(fname[,mode][,encoding])`  
`fo.[operation]`  
`fo.closed()  `

2. 工厂函数file()，和open功能相同，python3中已取消  
`fileObj=file(fname[, mode[,encoding]])`  
`fo.[operation]`  
`fo.closed()`  

3. 上下文管理器,无需手动close()  
`with open(fname[,mode][,encoding]) as fileObj:`  
    `fo.[operatioin]`  

### 无法读取的情况
1. 编码报错  
分析：   
文件里含中文，且文件本来的的编码和python读取文件所用的解码方式不同（其一在Windows下，记事本新建的文本文件采用的ANSI编码规则，即对不同的语言使用不同的编码方式：英文字符使用ASCII，中文字符使用GBK...其二python3新建文本文件采用的是MBCS编码（可以在IDLE中用 sys.getfilesystemencoding()查看），其原理也是基于ANSI，故对中文的编码总是默认gbk方式），在Windows下python读取文件的方式也就默认为MBCS，这样对于英文它会用ASCII无障碍读取，但是对于中文我就会用默认的gbk读取，如果文件是utf8编码而且有中文那就肯定会报错。  
解决：  
1.声明解码方式  
    `f=open(file,encoding='utf8')`  
2.修改文件编码方式  
    在Windows下使用记事本打开文件，然后另存为...就会看到文件的编码所采用的规则，可以自己更改。  
总之我们的目的就是使读取所用的编码与文件本身编码相同。
很有必要用图片演示一下   
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-b250500e8d92c1e022080d184be2b936_b.png?raw=true">
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-f0beb8b4627f5c14262b7f1ebdedcbe1_b.png?raw=true">
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-7ac420bec4725f57441a4bbb62be611c_b.png?raw=true">
（上图用记事本另存为的方式更改编码规则）

2. 打印空白  
1.写完之后未关闭文件  
没有f.close()
写错了 f.close   
写错了 f.closed    （这是用来判断文件是否关闭的布尔函数别搞混  
2.打开模式错了  
读写模式未分清      `f=open(file,'w')`    w模式为只写模式  
未注意文件指针位置  `f=open(file,'a+')`   虽然可读可写，但是为追加模式，注意文件指针在最后，故只能读取到空白，要想解决，可以调整指针到开头 f.seek(0) ，然后就可以读到了  
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-d561783a1f44258a6b4c9c56f7c450f9_b.png?raw=true">
有必要附图一张
<img src="https://github.com/clouduan/clouduan.github.io/blob/master/img/blog_pic/pth%E6%96%87%E4%BB%B6%E7%BC%96%E7%A0%81/v2-92236fdf9bc8348061111251711f3aee_b.png?raw=true">