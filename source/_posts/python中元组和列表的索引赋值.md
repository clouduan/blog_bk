---
title: python中元组和列表的索引赋值
date: 2017-07-25 13:32:08
tags:
- pythonic
categories:
- python语法笔记
---

+ 好的做法：

  ```python
  >>> x=(2,3)
  >>> a,b=x  # a=2,b=3

  >>> x=(2,3,4)
  >>> a,b=x[:2] # a=2,b=3
  ```
<!--more-->
+ 不好的做法：

  ```python
  >>> x=(2,3)
  >>> a,b=x[0],x[1]  # a=2,b=3

  >>> x=(2,3,4)
  >>> a,bx[0],x[1] # a=2,b=3
  ```

  ​