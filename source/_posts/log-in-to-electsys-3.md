---
title: Log in to electsys(3)
date: 2017-09-05 10:55:35
tags:
- 模拟登录
categories:
- 模拟登录选课系统
---

谁想昨天刚刚宣布破产，今早就成功登录了，于是迫不及待地来记录一下这成败的缘由。

<!--more-->

我最开始用的是`requests`，因为它极其方便。我按照登录协议一步步地实现，最后post表单之后需要经过几次跳转转到一个选课首页的网址http://electsys.sjtu.edu.cn/edu/student/sdtMain.aspx。这个网址返回的内容是这样的：

```html
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >

<HTML>
<HEAD>
	<TITLE>上海交通大学教学信息服务网－学生服务平台</TITLE>
	<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
	<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
	<meta name="CODE_LANGUAGE" Content="C#">
	<meta name="vs_defaultClientScript" content="JavaScript">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</HEAD>
<frameset rows="105,*" cols="*" frameborder="NO" border="0" framespacing="0">
	<frame src="../include/flattop.htm" name="topFrame" frameborder="no" scrolling="no" noresize marginwidth="0" marginheight="0">
	<frameset cols="125,*" frameborder="NO" border="0" framespacing="0">
		<frame src="sdtleft.aspx" name="leftFrame" frameborder="no" scrolling="no" noresize marginwidth="0" marginheight="0">
		<frame src="../newsboard/newsinside.aspx" name="main" frameborder="no" scrolling="auto" marginwidth="0" marginheight="0">
	</frameset>
</frameset>
<noframes>
</noframes>
</HTML>
```
我一贯的做法是在pycharm中新建一个HTML文件，然后把内容拷贝进去然后打开（pycharm会自动调用系统默认浏览器）。打开效果是这样的：

{%asset_img 2017-09-05 10-57-53.png %}

看到404 not found 我第一感觉就是请求有问题而且极可能是cookies的问题，于是我在浏览器登录之后导出了cookies然后加到程序里运行---如故！可根据5月份记录[第一次搞这个](http://clouduan.tk/2017/05/27/log-in-to-electsys-1/)的时候用cookies成功登录过的啊,难道我那时是直接访问课程页面并非选课首页？我看到在浏览器的请求里面有个`sdtleft.aspx`的请求（如图）：

{%asset_img 2017-09-05 11-24-513.png %}

于是我又把最后一步要获取的url改为http://electsys.sjtu.edu.cn/edu/student/sdtleft.aspx：

{%asset_img 2017-09-05 11-28-02.png%}

Bingo~

现在来回顾下我错误的原因：

查看我访问sdtMain.aspx时返回的源码，才发现这个一个框架啊！靠它来异步加载其它部分比如sdtleft.aspx，我直接打开的话没有cookies啥都没带，想要加载其余部分当然会是404啦！

如图，可以看到异步加载失败的过程：

{%asset_img 2017-09-05 14-50-45.png%}

下一步，该捣鼓如何刷课啦。