---
title: python-Http请求中的cookie处理
date: 2017-09-03 16:02:51
tags:
- urllib
- cookies
- requests
---

在模拟登录时，cookies一直是比较难搞的东西。对于Python来说有urllib和requests模块，本文记录两者对cookies的操作。

<!--more-->

## requests

requests的session会话可以跨请求保持参数，会在同一个Session实例发出的所有请求之间保持cookies。

> This module provides a Session object to manage and persist settings acrossrequests (cookies, auth, proxies)

用法

```
Basic Usage::
      >>> import requests
      >>> s = requests.Session()
      >>> s.get('http://httpbin.org/get')
      <Response [200]>

Or as a context manager::

      >>> with requests.Session() as s:
      >>>s.get('http://httpbin.org/get')
      <Response [200]>
```

上面例子中，如果要查看请求之后cookies的值可以直接:

```
s.cookies
```



如果我要添加自己的cookies怎么办？session的参数用户设置的优先度更高，因此可以在特定的会话前加入cookies：

```
>>> cookies = dict(cookiename='cookievalue')
>>> r = requests.get('http://httpbin.org/cookies', cookies=cookies)
>>> r.text
'{"cookies": {"cookies_are": "working"}}'
```

## urllib

不同与requests，没有session功能，不过可以使用cookieJar来自动处理cookies：

```
import http.cookiejar

cookie = http.cookiejar.CookieJar()
cookie_handler = urllib.request.HTTPCookieProcessor(cookie)
opener = urllib.request.build_opener(cookie_handler)

urllib.request.install_opener(opener)
```

上面例子中，如果要查看请求之后cookies的值可以这样：

```
for item in __cookie:
    if item.name == 'some_cookie_item_name':
        print item.value
```



同样地，如果自己需要加特定的cookies，由于cookies本来是headers的一部分，因此：

```
import urllib.requests
opener = urllib.requests.build_opener()
opener.addheaders.append(('Cookie', 'cookiename=cookievalue'))
f = opener.open("http://example.com/")
```

## P.S.

上面提到了一个url http://httpbin.org/ ，这个网址可以用来查看测试http请求。其使用方法的介绍
