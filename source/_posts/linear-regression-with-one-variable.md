---
title: 一元线性回归分析之LS算法
date: 2017-09-01 16:33:35
tags:
- 机器学习
- 线性回归
---

# 建模

假设两个变量之间是一元线性关系，则回归函数为$$h_{\theta}=\theta_0+\theta_1x$$

对应的Cost Function为$$J(\theta_0,\theta1)=\frac1{m}\sum_{i=1}^m(h_\theta(x^{(i)}-y^{(i)}))^2$$ 。

现在要寻找一种算法来优化损失函数---求解参数$\theta_0$和$\theta_1$，使该损失函数值最小。

采用的算法有梯度下降算法(Gradient Descent)和最小二乘法(Least Squares)。

<!--more-->

# 梯度下降算法

Ng对于该算法解释得颇为详细，在此不做记录，总结下要点就是八个字：迭代赋值，同步更新。

梯度下降算对我来说很新鲜，因为它的实现需要借助计算机强大的运算能力，所以课本上一般也不会提到。不过我觉得很多人都知道最小二乘法，因为我记得高中的数学课本里就有涉及到一元线性回归的问题，并且给出了最小二乘法求解参数的公式，大学微积分给出了最小二乘法的原理。

私以为“回归”和“最小二乘”的翻译颇不厚道，有种故作深沉的装逼之嫌。“回归”一词强行地和回归线类比，倒是有点“拟合”、“两侧分布”的意思，而“二乘”是什么鬼？“Least squares”译为“平方最小”是多么通俗易懂言简意赅！中国古代的符号系统极其残缺，却总喜欢在文字上作文章，以达到形式上的“美”。举个栗子就是“天元”啦，不知言之何物。

# 最小二乘法

除了梯度下降算法，Ng还给出了一种求解$\theta$的公式，他称之为Normal equation，实际上就是最小二乘法。

对损失函数求导
$$
\begin{cases}  
\frac{\partial J}{\partial \theta_0}=\frac2m\sum_{i=1}^m(\theta_0+\theta_1x_i-y_i) \\
\frac{\partial J}{\partial \theta_1}=\frac2m\sum_{i=1}^m(\theta_0+\theta_1x_i-y_i)x_i \\
\end{cases}
$$

令上式右边分别为0，来求拐点
$$
\begin {cases}
m\theta_0+(\sum_{i=1}^mx_i\theta_1=\sum_{i=1}^my_i \\
(\sum_{i=1}^mx_i\theta_0+(\sum_{i=1}^mx_i^2)\theta_1=\sum_{i=1}^mx_iy_i \\
\end {cases}
$$
这是一个二元方程组，其系数行列式：
$$
\begin {vmatrix}
m & \sum_{i=1}^mx_i \\
\sum_{i=1}^mx_i & \sum_{i=1}^mx_i^2
\end {vmatrix}
=m \sum_{i=1}^mx_i^2-(\sum_{i=1}^mx_i)^2
$$
而
$$
m \sum_{i=1}^mx_i^2-(\sum_{i=1}^mx_i)^2=m^2(\frac1m \sum_{i=1}^mx_i^2-( \frac1m\sum_{i=1}^mx_i)^2=m^2(\overline {x_i^2}-{\overline x_i}^2)
$$
上式右边括号内分别是平方平均数和算术平均数的平方，由于$x_i$不全相同，故该方程组有唯一解（另一种推导过程见Postscript）。

方程的解为（推导过程见Postscript）：
$$
\begin{cases}
\theta_1=\frac{m\sum_{i=1}^m x_iy_i-(\sum_{i=1}^mx_i)(\sum_{i=1}^my_i)}{m \sum_{i=1}^mx_i^2-(\sum_{i=1}^mx_i)^2}=\frac{\sum_{i=1}^m(x_i-\overline x)(y_i-\overline y)}{\sum_{i=1}^m(x_i-\overline x)^2} \\
\theta_0=\overline y-\theta_1\overline x \\
\end{cases}
$$

为方便，有时作如下记法：
$$
\begin{cases}
S_{XX}=\sum_{i=1}^m(x_i-\overline x)^2=\sum_{i=1}^m{x_i}^2-\frac1m(\sum_{i=1}^mx_i)^2=\sum_{i=1}^m{x_i}^2-m(\overline x)^2 \\
S_{YY}=\sum_{i=1}^m(y_i-\overline y)^2=\sum_{i=1}^m{y_i}^2-\frac1m(\sum_{i=1}^my_i)^2=\sum_{i=1}^m{y_i}^2-m(\overline y)^2 \\
S_{XY}=\sum_{i=1}^m(x_i-\overline x)(y_i-\overline y)=\sum_{i=1}^mx_iy_i-\frac1m(\sum_{i=1}^m{y_i})(\sum_{i=1}^m{x_i})=\sum_{i=1}^mx_iy_i-m\overline x\overline y \\
\end{cases}
$$
则方程的解可以表示为：
$$
\begin{cases}
\theta_1=\frac {S_{XY}}{S_{XX}} \\
\theta_0=\overline y-\theta_1\overline x \\
\end{cases}
$$


# 最小二乘法矩阵形式

Ng在课堂上推荐用向量和矩阵来计算，因为Matlab/Octave矩阵运算非常快，而且在许多应用中具有数千个特征变量的机器学习是很常见的。所以很必要的一部就是向量化(Vectorization)。

所以先要把模型函数构造成向量的形式，我体会到的构造方法有两点：其一就是由小到大，层层构造；其二就是利用好矩阵乘法中 [m * p] x [p * n]=[m * n]。模型函数的矩阵表示为：
$$
J=\frac1m({\bf X}{\bf \theta}-{\bf y})^T({\bf X}{\bf \theta}-{\bf y})
$$
其中
$$
{\bf y}=\begin{bmatrix}y_1\\y_2\\...\\y_m\end{bmatrix}, {\bf X}=\begin{bmatrix}1&x_1\\1&x_2\\...\\1&x_m\end{bmatrix},{\bf \theta}=\begin{bmatrix}\theta_1\\\theta_2\\...\\\theta_m\end{bmatrix}
$$
为了得到$J$的极小值，我们仍然要求其拐点，仍然需要求偏导数$ \frac {\partial J}{\bf \partial \theta}$。

我先将$J$展开：
$$
J =\frac1m({\bf X}{\bf \theta}-{\bf y})^T({\bf X}{\bf \theta}-{\bf y}) \\\quad\quad
=\frac1m(({\bf X}{\bf \theta})^T-{\bf y}^T)({\bf X}{\bf \theta}-{\bf y}) \\\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad
=\frac1m({\bf X}{\bf \theta})^T{\bf X}{\bf \theta}-\frac1m{\bf y}^T{\bf X}{\bf \theta}-\frac1m({\bf X}{\bf \theta})^T{\bf y}+\frac1m{\bf y}^T{\bf y} \\\quad\quad\quad\quad\quad\quad
=\frac1m({\bf \theta})^T{\bf X}^T{\bf X}{\bf \theta}-\frac2m{\bf y}^T{\bf X}{\bf \theta}+\frac1m{\bf y}^T{\bf y} \\
$$
求偏导有两种做法：其一是可以将上式展开，求得$J$关于每个参数$\theta$的导数再组成一个向量，太烦了；其二是利用一些向量微分公式，这些公式本身也很好证明的。
$$
\begin{array}  
{c | c}  
f(\theta) &\frac{\partial f}{\partial \theta} \\  
\hline 
\theta^Tx & x \\  
\hline 
x^T\theta&x  \\
\hline
\theta^T\theta&2\theta \\
\hline
\theta^TC\theta&2C\theta \\
\end{array}
$$
于是
$$
\frac {\partial J}{\bf \partial \theta}=\frac2m{\bf X}^T{\bf X}{\bf \theta}-\frac2m{\bf X}^T{\bf y}=0
$$
化简得到
$$
{\bf X}^T{\bf X}{\bf \theta}={\bf X}^T{\bf y}
$$
所以
$$
{\bf \theta}=({\bf X}^T{\bf X})^{-1}{\bf X}^T{\bf y}
$$
这就是Ng在给出的公式，也就是最佳参数值！

# Postscript

$m \sum_{i=1}^mx_i^2-(\sum_{i=1}^mx_i)^2\==m\sum_{i=1}^m(x_i-\overline x)^2$的推导：
$$
m \sum_{i=1}^mx_i^2-(\sum_{i=1}^mx_i)^2\\ 
=m[\sum_{i=1}^mx_i^2-\frac1m (\sum_{i=1}^mx_i)^2]\\\quad\quad\quad\quad\quad\ \;
=m[\sum_{i=1}^mx_i^2-\frac2m (\sum_{i=1}^mx_i)^2+\frac1m (\sum_{i=1}^mx_i)^2]\\\quad\quad\quad\quad\quad\quad\quad\quad\quad \quad
=m[\sum_{i=1}^mx_i^2-2 (\sum_{i=1}^mx_i)(\frac1m\sum_{i=1}^mx_i)+m (\frac1m \sum_{i=1}^mx_i)^2]\\\quad \;
=m[\sum_{i=1}^mx_i^2-2 (\sum_{i=1}^mx_i)\overline x+m{\overline x}^2]\\\quad\quad \;
=m[\sum_{i=1}^mx_i^2-2 (\sum_{i=1}^mx_i)\overline x+\sum_{i=1}^m{\overline x}^2]\\
=m\sum_{i=1}^m(x_i-\overline x)^2\quad\quad\quad\quad\quad
$$




