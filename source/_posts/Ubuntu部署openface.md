---
title: Ubuntu部署openface
date: 2017-07-25 13:11:53
tags:
- face recognition
- openface
- CV
- Ubuntu
- cuda
categories:
- face recognition
---
+ Website: http://cmusatyalab.github.io/openface/    
+ github:https://github.com/cmusatyalab/openface   
+ installation:http://cmusatyalab.github.io/openface/setup/   

<!--more-->
## Environment  
+ Ubuntu14.04 

## Customized  

1. 首先安装[Ubuntu14.04](http://www.ubuntu.org.cn/download/alternative-downloads/)，选择镜像下载。然后刻盘安装，我安的双系统，之后的安装全部要`sudo`  

2. 安装系统后需要安装一系列必备工具：

   ```bash
   sudo apt-get update
   sudo apt-get upgrade
   sudo apt-get install ssh git curl zsh vim zip wget
   ```

3. 然后要安装必要的依赖：

   ```bash
   sudo apt-get install build-essential cmake gfortran  libatlas-dev libavcodec-dev libavformat-dev libboost-all-dev libgtk2.0-dev libjpeg-dev liblapack-dev libswscale-dev pkg-config python-dev python-pip
   ```

4. 根据[官网](http://cmusatyalab.github.io/openface/setup/)上的说明 ：

   > This project uses `python2` because of the `opencv` and `dlib` dependencies. Install the packages the Dockerfile uses with your package manager. With `pip2`, install `numpy`, `pandas`, `scipy`, `scikit-learn`, and `scikit-image` 

   需要安装以上科学计算工具包：

   ```bash
   sudo pip2 install numpy  
   sudo pip2 install scipy  
   sudo pip2 install pandas  
   sudo pip2 install scikit-learn  
   sudo pip2 install scikit-image
   ```

5. 安装Torch：基于lua的科学计算工具，其包管理器是luarocks ：

   [官方安装说明](http://torch.ch/docs/getting-started.html#_)    

   先安装torch：  

   ```bash
   git clone https://github.com/torch/distro.git ~/torch --recursive  
   cd ~/torch    
   bash install-deps     
   sudo ./install.sh    
   source ~/.bashrc
   ```

   torch安装完成，用`th` 命令 查看。 

   然后用luarocks安装下列依赖：

   ```bash
   dpnn  
   nn  
   optim  
   csvigo  
   cutorch and cunn (only with CUDA)  
   fblualib (only for training a DNN)  
   tds (only for training a DNN)  
   torchx (only for training a DNN)  
   optnet (optional, only for training a DNN) 
   ```

   我用`luarocks install $NAME` 和`sudo luarocks install $NAME` 都安装失败，最终在 clone的torch/install/bin里面，找到一个包含luarocks的目录，cd进去用如下命令安装： 
   `sudo ./luarocks install $NAME` 
   其中cuda未安装成功，因为版本号不匹配的原因，白装了NVIDIA驱动，最后又卸了没装；fblualib安装失败，在github上直接clone源码编译安装了,在安装之前先要保证安装(mstch)[https://github.com/no1msd/mstch]和(zstd)[http://progur.com/2016/09/how-to-install-and-use-zstd-facebook.html]。 
   安装完成用`luarocks list` 查看已安装的工具。   

6. 安装opencv：

   [官方安装说明](http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html)   
   [下载地址](http://opencv.org/releases.html)  

   下载后在相应的目录下执行命令：

   ```bash
   unzip  opencv-2.4.11.zip  
   cd  opencv-2.4.11  
   mkdir  release  
   cd  release  
   cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..  
   make    
   make  install  
   ```

   在Python2中用 `import cv2` 验证

7. 安装dlib：

   [安装说明](http://cmusatyalab.github.io/openface/setup/)   
   [下载地址](https://github.com/davisking/dlib/releases)    
   下载后在相应的目录下执行命令：  

   ```bash
   tar xf dlib-18.16.tar.bz2    
   cd dlib-18.16/python_examples     
   mkdir build    
   cd build    
   cmake ../../tools/python    
   cmake --build . --config Release    
   cp dlib.so /usr/local/lib/python2.7/dist-packages
   ```

   在Python2中用`import dlib` 验证。   

8. 安装openface：

   Git获取openface： 

   ```bash
   git clone https://github.com/cmusatyalab/openface.git  
   cd openface
   git submodule init  
   git submodule update 
   ```

   编译安装：  

   ```bash
   sudo python2 setup.py install
   ```

   获取模型： 

   ```bash
   cd openface/models/
   sudo ./get-models.sh
   ```

   运行demos：

   ```bash
   cd ../demos
   #demo1
   sudo ./compare.py  ../images/examples/{lennon*,clapton*}
   #demo2
   sudo ./classifier.py infer models/openface/celeb-classifier.nn4.small2.v1.pkl   ../images/examples/carell.jpg
   #demo3
   cd web   
   sudo./web/start-servers.sh
   #demo4
   cd demos
   sudo ./sphere.py
   ```

   ​

## 2017/6/29 Update: th 路径    
运行demo1时提醒 `!# usr/bin/env:th:` 找不到目录。于是查看了 `compare.py ` 文件，并且Google得知了 `env` 是指环境变量，比如`!# usr/bin/env python2` 意思就是在环境变量里找Python解释器。这里我用 `which th` 查看th的路径，显示是在 `torch/install
/bin`里（就是我clone的torch库里），那怪不得找不到了。又Google得知：用如下命令把路径进行链接就可以了：

```bash
ln -s /home/clouduan/torch/install/bin/th /usr/local/bin/th`
```

  

## 2017/6/30/Update:cuda/cunn&cutorch installed    
`sudo ./luarocks install cunn` 安装失败大致有以下几点原因：  
(1)未安装cuda；(2)安装的cuda版本不支持；(3)未指明cuda的路径。  
我先是未安装cuda，编译时报错`Specify CUDA_TOOLKIT_DIR`，之后胡乱捣腾了下安了cuda（没按照标准方法安装），又报错说我的是5.5版本，这个至少要6.5，可我当时明明装了cuda-8.0（在/usr/local/里看到的）。我又循着报错的文件`FindCUDA.cmake` 进行查看，说要 `Specify CUDA_TOOLKIT_DIR` ，于是又在`~/.bashrc` 中添环境变量，但还是失败，且一直是这个错误。  
最后觉得应该要重装cuda了。  于是先删掉了所有的cuda相关。找到了[官方安装](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#pre-installation-actions)说明，写得超级详细，其实够用了；网上也搜了几篇，都几乎是翻译版而且都是转载的同一人的。其中[这篇](http://blog.csdn.net/l297969586/article/details/53320706)值得推荐。  
先去官网，我首选的`runfile`本地安装，其包括了OpenGL，NVIDIA驱动，cuda toolkit 以及 samples。   
(1).  查看设备信息等，选择对应的版本下载：推荐用百度云离线下载（这个功能不错的），然后检验md5值。 
(2).  安装所需的组件：   
(3).  关闭`nouveau` ：这是一个由独立程序员为NVIDIA开发的开源显卡驱动，不属于NVIDIA，所以不能兼容cuda，因此必须关闭。注意要查看是否成功关闭，我的情况是需要重启电脑再输命令查看，不重启的话还是会看到输出。另外[这篇](http://blog.csdn.net/l297969586/article/details/53320706)中此处也很详细。  
(4).  然后按`ctrl+alt+f1` 进入`tty` ,登录之后 关闭`lightdm`服务：   
`sudo service lightdm stop`   
在run文件的目录下 执行   
`sudo sh cuda****_linux-run`   
进行安装，首先会出来一个说明文档之类的东西，需要按回车读完它（注意左下方会有进度数字，那个是指读文档的进度，不是安装的进度），然后就会出现安装的提示，按照提示安装即可。注意选择是否安装`OpenGL` 时选否，网上说的不明照做；注意如果出现空间不足的情况，网上有人说后面加个 `--tmpdir=/opt/tmp` ，这就是弄了个临时缓冲文件夹，对我没用，因为我本来就是要往`/.` 里面装的。我最后试了下，先装驱动（第一遍别选toolkit），然后再装一次（第二次选toolkit别选驱动，samples用不着也没选），最后顺利安装。也成功安装了 cunn和cutorch，撒花（倒地...），电脑严重发热。   
(5).  安装后重启，发现分辨率竟然变了，界面及其模糊。我用 `cvt`+`xrandr`命令只能暂时改过来，最后是找到了[这篇教程](https://www.douban.com/note/262885853/)才改过来的，注意原作者所提到的三处都要改，改完重启，恢复正常。这才算一切搞定。  

## 2017/6/30/Update:opencv reinstalled     
在运行[Demo4](https://cmusatyalab.github.io/openface/demo-4-sphere/) 时出现 `'module' object has no attribute 'CV_AA'` 的错误。其原因是当时配置时太贪心安了opencv3，而opencv3相对于2版本变化较大，
>From OpenCV 2.X OpenCV 3.0 a few things changed.   
>Specifically:   
> * cv2.cv doesn't exists in OpenCV 3.0. Use simply cv2.  
> * some defines changed, e.g. CV_BGR2HSV is now COLOR_BGR2HSVif    you're using opencv3, try cv2.LINE_AA instead.     

我选择重装，先[卸载](http://blog.csdn.net/yoouzx/article/details/52988094)，然后下载2版本的安装即可。

## 2017/7/30/Update:Re-deployed on Ubuntu16.04LTS

#### 安装cuda toolkit

安装时遇到如下问题：

```
The driver installation is unable to locate the kernel source.Please make sure that the kernel source packages are installed and set up correctly. If you know that the kernel source packages are installed and set up correctly, you may pass the location of the kernel source with the '--kernel-source-path' flag.
```

有人说要安装内核头文件，但我之前确实是安装了头文件啊[[Ref](https://ubuntuforums.org/showthread.php?t=843914)]：

```
sudo apt-get install linux-headers-$(uname -r)
```

有人说应该显式指明kernel source的位置：

```
sudo sh cuda***_linux.run --kernel-source-path=/usr/src/linux-headers-***-generic
```

但是看到网上的人说不管用，我也就暂且没试。

有人说应该先安装dkms(动态内核模块支持)，再安装cuda toolkit[[Ref](https://unix.stackexchange.com/questions/115289/driver-install-kernel-source-not-found)]：

```bash
sudo apt-get install dkms
sudo sh cuda_***_linux.run
```

激动，竟然成功了~

事后，有人采取先装driver，然后装toolkit并且装toolkit时在driver的一项中选no，也成功了，不过看起来好麻烦[[Ref](https://devtalk.nvidia.com/default/topic/978812/installing-cuda-7-5-fails-on-ubuntu-14-0-4-5-with-error-driver-installation-is-unable-to-locate-the-kernel-source/)]。

安装cuda时这篇老外的文章对于各种原理的理解帮助很大，文风也幽默：[[Ref](https://www.linkedin.com/pulse/installing-nvidia-cuda-80-ubuntu-1604-linux-gpu-new-victor)]

#### 安装opencv

上次是先装了cuda然后装opencv，所以opencv一路安装顺利；这次是先装了cuda，这下问题就来了。

1. 首先就遇到一问题，沿着error一路找到源头，信息如下：

```
 /data/opencv-2.4.11/modules/gpu/src/graphcuts.cpp:120:54: error: ‘NppiGraphcutState’ has not been declared  
      typedef NppStatus (*init_func_t)(NppiSize oSize, NppiGraphcutState** ppStat  
```

Google之后才知道是由于cuda8.0太新，open2.4太老，所以兼容不上，因此需要找到`opencv2.4.11/modules/gpu/src/graphicuts.cpp`，打开并改动一行：

```cpp
#if !defined (HAVE_CUDA) || defined (CUDA_DISABLER)  
```

 改为

```cpp
#if !defined (HAVE_CUDA) || defined (CUDA_DISABLER) || (CUDART_VERSION >= 8000) 
```

这样就可以兼容8.0版本啦。

2. 然后又报错，不支持的gpu架构：

```
Building NVCC (Device) object modules/core/CMakeFiles/cuda_compile.dir/src/cuda/Debug/cuda_compile_generated_gpu_mat.cu.obj

nvcc fatal : Unsupported gpu architecture 'compute_11'
```

需要用**CUDA_GENERATION**这个参数指定GPU显卡的架构，我的显卡是GeForce610M，为Kepler架构，[[Ref](https://en.wikipedia.org/wiki/GeForce_600_series)]，编译成功[[Ref](https://stackoverflow.com/questions/28010399/build-opencv-with-cuda-support)]。

## Reference  
+ http://blog.csdn.net/itas109/article/details/50790139  
+ http://shamangary.logdown.com/posts/800267-openface-installation  
+ http://www.cnblogs.com/pandaroll/p/6590339.html  
+ http://blog.sina.com.cn/s/blog_16a9a48a30102xd8x.html  
