---
title: NLP-语言处理与python
date: 2017-07-25 12:09:02
tags: 
- python
- NLP
categories:
- 《python自然语言处理》学习笔记
---
from nltk.book import *  
book模块包含9个文本数据（text1~9）和对语料库相应的操作函数  
nltk中的文本是一个词汇的列表，当然可以用列表的操作来获取相应的东西    
text1[1374]  
text1.index('word')  
len(text4)   
set(text3)   
<!--more-->
## 1.搜索文本 

```python
text1.concorance('word')   
会显示每个包含该词的句子   
text2.similar('word')   
和word出现在相似的上下文中(a *hot* day;a *beautiful* day)   
text3.common_contexts(['word1','word2','word3'...])   
共用word1,word2,word3...的上下文   
text4.dispersion_plot(['word1','word2','word3'...])    
word1,word2...在text4中的出现位置分布图   
```

## 2.计数词汇   
```
常用 len() sorted()  set()    
len(text4)   
词汇数    
set(text3)   
不重复的词汇集合    
```

## 3.频率分布   
| 函数用法                        |                  功能描述                   |
| --------------------------- | :-------------------------------------: |
| fdist=FreqDist(sample)      | 创建sample的频率分布，fdist是一个 词汇-出现次数 的类似字典的东西 |
| fdist.inc(sample)           |                  增加样本                   |
| fdist['word']               |                word的出现次数                |
| fdist.freq('word')          |                word的出现频率                |
| fdist.N()                   |                  样本总数                   |
| fdist.keys()                |               频率递减排列的样本链表               |
| for sample in fdist         |                频率递减遍历样本                 |
| fdist.max()                 |            数值最大的样本，即出现次数最多的词            |
| fdist.tabulate()            |                 绘制频率分布表                 |
| fdist.plot()                |                 绘制频率分布图                 |
| fdist.plot(cumulative=True) |                绘制累计频率分布图                |

## 4.词汇比较    
| python字符串操作函数   |    功能     |
| --------------- | :-------: |
| s.startswith(t) |  是否以t开头   |
| s.endswith(t)   |           |
| t in s          |           |
| s.islower()     |           |
| s.isupper()     |           |
| s.isalpha()     |           |
| s.isalnum()     | 是否都是字母或数字 |
| s.isdigit()     |  是否都是数字   |
| s.istitle()     | s是否首字母大写  |

**附：[代码练习](https://github.com/clouduan/practice/blob/master/nlp-learning/ex1.py)**
