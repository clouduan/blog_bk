---
title: requests获取页面乱码的问题
date: 2017-08-14 23:27:09
tags:
- requests
- Web Crawler
- 编码
---
暑假在家里呆几天，没了WiFi，反而让我有动力和心境去把以前开的坑填掉。昨天到今天刷完了一套HTML教程和两套JavaScript教程，构思了chrome选课插件的基本架构，还取了名字叫ElectHelper（原先想取个LoopHander来着，取刷课能手之意，后来想了想还是那个更切题），等待开学第三轮选课时进行完善，填其一坑也。从bitbucker上整理了qqbot的repo，开发自己的qqbot，欲填第二坑。

<!--more-->
说是没WiFi，其实还是有的。镇子上了人安全意识真是淡薄，WiFi密码用一百年都不换的好像。那密码早就被人分享到了网上（没错就是万能钥匙），我有幸蹭一下，不过可能因为距离太远吧，网速颇有些蛋疼。

在测试qqbot时要获取二维码和appid等一些东西，发现用requests总是会出现乱码，而且我也不知道怎么手动指定网页编码：
```
url='https://ui.ptlogin2.qq.com/cgi-bin/login?daid=164&target=self&style=16&mibao_css=m_webqq&appid=501004106&enable_qlogin=0&no_verifyimg=1&s_url=http%3A%2F%2Fw.qq.com/proxy.html&f_url=loginerroralert&strong_login=1&login_state=10&t=20130723001&f_qr=0'
res=requests.get(url)
res.text 
html=res.text
print(html)  # 输出网页，就会出现网页乱码
```

而用urllib库获取页面，由于我知道怎么指定编码，不会出现这个问题：

```
html=urllib.request.urlopen(url).read().decode("utf-8")
```

上网搜到如何给requests添加编码指定：

### 方法一

```
res.encoding="utf-8"
html=res.text
```

### 方法二

原始内容在 `res.content` 里，是`bytes`，自己想怎么处理就怎么处理：

```
html=res.content.decode("utf-8")
```

由方法二还可以引出另一个问题就是 `response.content`和`response.text`的不同之处。
