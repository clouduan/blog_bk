---
title: Log in to electsys(1)
date: 2017-05-27 12:28:48
tags:
- 模拟登录
categories:
- 模拟登录选课系统
---

前几天开放选课了，下学期的课是重要的，因此有了抢课需求---实现循环刷课自动化，“我爱选课”插件用的js、jquery看起来一头雾水，也不能要求插件作者去做这个---毕竟方便大家的东西，恶性竞争就变了味。
网上搜了下原理，看起来还是简单的。不过昨天上手试了下，对网络很多东西不熟悉，还是有难度。所以记录下来，是为实验报告。

<!--more-->

## 分析

### 说明

教学信息服务网登录地址 http://electsys.sjtu.edu.cn/edu/index.aspx ,学校已取消了账号密码登录，统一用jaccount登录。坏处：要做验证码识别，之后又要做图像处理分析；好处：只要通过认证，交大其它用jaccount登录网站的就不需要再登录，一劳永逸。

### 验证码获取

发出get请求，得到response并以二进制保存验证码为PNG图片格式，再自动打开。
构造一个opener，其handler可以自动管理cookie（对cookie还是很不熟悉

### 构造表单并编码

`f12`-`network`，根据formData构造表单，其中的sid就是sessionid，进行网站与用户身份识别，user、pass，captcha是验证码，由外部传入。其余的每次都不变，直接写入。用urlencode编码后还要用encode弄成bytes格式

### 发送请求进入个人页面

参照`f12`进行post，虽然http状态码返回的是200，但是对返回的网页压缩包进行解压解码后看到是验证码错误,几番如此。网上有重定向，还有验证码和cookie是同步的等好多解释。
首先的确是有重定向（太广泛了），于是我在每次post完成后没有打开返回的网页，而是直接去访问个人页面 http://electsys.sjtu.edu.cn/edu/student/sdtMain.aspx ，但发现它总是给我重定向到教学信息服务网首页：这和浏览器在未登录情况下直接访问个人页面的结果是一样的，这说明肯定是验证的问题。鉴于对cookiejar的操作不熟，遂直接把浏览器的cookie保存了下来，打开读取cookie(json格式)并更新headers,果然登入了个人页面。

### 发送请求进入选课页面

开着`f12`,打开`一专选课`/`海选`,看到点击`我已阅读`，看到有post表单，并且get了选课页面。这次不打算再构造编码了，直接复制 unparse的字符，在前面加“b”转换成bytes就是了，发送post请求并get选课页面（此过程需要更新cookies，不然会返回网页过期，直接浏览器的cookies即可），至此算是才成功进入了选课页面。

### 选课

每次点击按钮进行选课都是post操作，formData中不同的课程对应着不同的id，依次走流水账，模拟了一门课的选课过程。

------

## 2017/5/28 更新

今天重新试了一遍竟然登录失败了！不过也在情理之中，流程太渣。所以今天决定推倒重写。

首先就是不用urllib库了，requests够好的，而且session对话能够自动追踪cookies，不需要麻烦用cookjar构造opener了。又试了下，连header也免了。
而且原来表单中的某些东西是在网页源码中的，不能乱填，所以用re找出来。	

get了一种cookies转换的操作：
`#先是加载cookies  
with open('cookies.txt') as f:  

```
cookies_dict={}  
for line in f.read().split(';'):  
	name,value=line.strip().split('=',1)  
	cookies_dict[name]=value`  
```

`#然后转换cookies   
cookies=requests.utils.cookiejar_from_dict(cookies) `  

试了下，表单还是要一步步提交，不能一下子提交母的的表单（那门课的表单信息）

## 2017/5/29 更新

忙活了三四天的选课脚本，今天正式宣布放弃了。终于明白原来自己并没有真正地模拟登录进去，之前能够登录，全是cookies的功劳。

试了下就算不弄验证码，不提交任何表单，只要在get请求中加入cookies，就可以登录进去---枉我费了那么多心思，在无数的重定向间徘徊

**神tm的重定向，动态网站重定向太多了，已弃坑。**

不过cookie的话，脚本更简单了，没有太多的技术难题，刷课还是可行的。](http://clouduan.tk/2017/09/03/python-urllib%E9%AB%98%E7%BA%A7%E7%94%A8%E6%B3%95/)