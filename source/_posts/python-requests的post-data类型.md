---
title: python-requests的post-data类型
date: 2017-09-03 22:29:19
tags:
- requests
---

我在使用requests的过程中，发现在构造post请求时，有时传递的data参数是一个json（比如在SmartQQ登录协议中），有时又是一个字典（这类比较常见）。而且每次使用似乎是直接传进去，并不像`urlopen`一样经过`urlencode`编码操作，查了[文档](http://docs.python-requests.org/zh_CN/latest/user/quickstart.html)才明白post中的编码事项。

<!--more-->

在模拟登录时一般会带上headers，headers中有一项叫`content-type`，requests-post传递data的类型正与此有关。

## 编码的情况

普通的http的post请求的请求`content-type`类型是：`Content-Type:application/x-www-form-urlencoded` 意味着这个请求的data是要经过编码的。 这种情况要在请求头headers中声明`Content-Type=application/x-www-form-urlencoded`

### 传字典

> 通常，你想要发送一些编码为表单形式的数据——非常像一个 HTML 表单。要实现这个，只需简单地传递一个字典给 data 参数。你的数据字典在发出请求时会自动编码为表单形式

```
>>> payload = {'key1': 'value1', 'key2': 'value2'}

>>> r = requests.post("http://httpbin.org/post", data=payload)
>>> print(r.text)
{
  ...
  "form": {
    "key2": "value2",
    "key1": "value1"
  },
  ...
}
```

> 此处除了可以自行对 `dict` 进行编码，你还可以使用 `json` 参数直接传递，然后它就会被自动编码。这是 2.4.2 版的新加功能：

也就是可以把字典传给json参数，这和后面的**传递json形式**不矛盾，因为后者是把dict转化为string传给data参数。

```
>>> url = 'https://api.github.com/some/endpoint'
>>> payload = {'some': 'data'}

>>> r = requests.post(url, json=payload)
```

### 传元组

> 你还可以为 `data` 参数传入一个元组列表。在表单中多个元素使用同一 key 的时候，这种方式尤其有效：

```
>>> payload = (('key1', 'value1'), ('key1', 'value2'))
>>> r = requests.post('http://httpbin.org/post', data=payload)
>>> print(r.text)
{
  ...
  "form": {
    "key1": [
      "value1",
      "value2"
    ]
  },
  ...
}
```

## 不编码的情况

另一种`Content-Type`为`application/json`，意味着发送的数据不编码。这种情况要在请求头headers中声明`Content-Type=application/json`

> 很多时候你想要发送的数据并非编码为表单形式的。如果你传递一个 `string` 而不是一个 `dict`，那么数据会被直接发布出去。

可以使用`json.dumps()`将将dict转化为json字符串：

```
>>> import json

>>> url = 'https://api.github.com/some/endpoint'
>>> payload = {'some': 'data'}

>>> r = requests.post(url, data=json.dumps(payload))
```



综上，编码则传dict|tuple；不编码则传json字符串(由dict转化而来)