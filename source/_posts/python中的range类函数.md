---
title: range() | xrange() | arange()
date: 2017-07-25 13:35:30
tags:
- pythonic
- NumPy
categories:
- python语法笔记
---
range():python BIF自带

xrange():python2版本独有，生成一个生成器（提升速度），其余功能和range()一样。

arange():是numpy库中的函数，返回一个数组对象，支持以float作为步长。
<!--more-->  

  ```python
  >>> a=xrange(5)
  >>> type(a)
  <type 'xrange'>
  >>> for i in a:
  	print i
  0
  1
  2
  3
  4
  ```

  ```python
  >>> a=numpy.arange(0,2,0.5)
  >>> a
  array([ 0. ,  0.5,  1. ,  1.5])
  >>> list(a)
  [0.0, 0.5, 1.0, 1.5]
  >>> type(a)
  <class 'numpy.ndarray'>
  >>> print([i for i in a])
  [0.0, 0.5, 1.0, 1.5]
  ```

  这里顺便提下生成器元素的输出方法：

  ```python
  >>> print(i for i in a)
  <generator object <genexpr> at 0x03EC0180>
  >>> print(i) for i in a
  SyntaxError: invalid syntax
  >>> print([i for i in a])
  [0.0, 0.5, 1.0, 1.5]
  ```

  上面代码中，第一个输出的只是一个`i for i in a`这个生成器；第二个语法错误；正确的做法是第三种，先把生成器转换为列表，然后输出（这好像有点缘木求鱼了啊）。
