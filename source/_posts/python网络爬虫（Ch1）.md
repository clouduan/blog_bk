---
title: 网络爬虫简介
date: 2017-08-07 00:26:59
tags:
- Web Crawler
- python
categories:
- 《用python写网络爬虫》学习笔记
---

《用python写网络爬虫》第一章

要点：
对5xx错误递归调用函数下载网页；避免重复爬取相同的链接；避免陷入死循环；支持代理；下载限速。

<!--more-->

## 检查robots.txt：

大多数网站会定义robots.txt文件，放在网站根目录下，以便让爬虫了解爬去该网站时存在哪些限制。

比如要查看baidu的robots.txt文件，可以访问http://baidu.com/robos.txt获得：

```python
User-agent: Baiduspider
Disallow: /baidu
Disallow: /s?
Disallow: /ulink?
Disallow: /link?

User-agent: Googlebot
Disallow: /baidu
Disallow: /s?
Disallow: /shifen/
Disallow: /homepage/
Disallow: /cpro
Disallow: /ulink?
Disallow: /link?
...
```

### User-agent

用以匹配受该协议约束的爬虫，*代表对所有的爬虫有效。

### Disallow

表示禁止爬虫访问的URL，这个URL可以是绝对路径也可以是相对路径。Disallow为空表示无限制，Disallow:/表示拦截整站。

### Allow

表示允许爬虫访问的目录，用法同Disallow。

### Sitemap

指定sitemap的位置。sitemap提供了所有网页的链接，可以帮助爬虫定位网站的最新内容。

### Crawl-delay

告诉爬虫两次访问的间隔，单位是秒，以免服务器过载。

### 例子

$匹配URL的末尾，*为通配符

#### 拦截.gif文件

```
User-agent:*
Disallow:/*.gif$ 
```

#### 拦截带有?的文件

```
User-agent:*
Disallow:/*?
```

## 识别网站所用的技术

用builtwith模块，python2有，python3无

```shell
pip2 install builtwith
```

```
>>> import builtwith
>>> builtwith.parse('http://example.webscraping.com')
{u'javascript-frameworks': [u'jQuery', u'Modernizr', u'jQuery UI'], u'web-frameworks': [u'Web2py', u'Twitter Bootstrap'], u'programming-languages': [u'Python'], u'web-servers': [u'Nginx']}
```

可以看出，示例网站使用率python的web2py框架，另外还是用了一些通用的js库，因此该网站的内容很有可能是嵌在HTML中的，相对来说比较容易抓取。

## 寻找网站的所有者

使用WHOIS协议查询网站域名的注册者

```
pip install python-whois
```

```
>>> import whois
>>> print whois.whois('python.org')
{
  ...
  "name_servers": [
    "NS3.P11.DYNECT.NET", 
    "NS1.P11.DYNECT.NET", 
    "NS2.P11.DYNECT.NET", 
    "NS4.P11.DYNECT.NET"
  ], 
  "org": "Python Software Foundation", 
  "creation_date": "1995-03-27 05:00:00", 
  "emails": [
    "e89d6901ba3e470e8cedc3eaa32a0074-1697561@contact.gandi.net", 
    "7fd79ebe18980da9baceef0fa1b27680-1705778@contact.gandi.net"
  ]
}
```

可以看出该域名所属的公司等。

## 编写第一个网络爬虫

### 下载网页

下面的代码下载网页，包含了试错机制和对网站返回5xx错误的处理。5xx错误发生在服务端，当download函数在遇到5xx错误码时，会递归调用函数自身进行调试，默认再重新下载两次。

*hasattr*(object, name) 判断一个对象里面是否有name属性或者name方法，返回BOOL值，有name特性返回True， 否则返回False

```
import urllib.request

def download(url,num_retries=2):
    print('downloading,',url)
    try:
        html=urllib.request.urlopen(url).read().decode('utf8')
    except urllib.request.URLError as e:
        print('downloading error:',e.reason)
        html=None
        if num_retries>0:
            if hasattr(e,'code') and 500<=e.code<600:
                return download(url,num_retries-1)
    return html

url='http://httpstat.us/500'
download(url)
```

### 网站地图爬虫

使用sitemap.xml来下载所有网页

```
import re
def crawl_sitemap(url):
    sitemap=download(url)
    links=re.findall('<loc>(.*?)</loc>',sitemap)
    for link in links:
        html=download(link)
        #...

url='http://example.webscraping.com/sitemap.xml'
crawl_sitemap(url)
```

### ID遍历爬虫

一般情况下，web服务器对于如下两种URL的识别是相同的：

http://example.webscraping.com/places/default/view/Afghanistan-1

http://example.webscraping.com/places/default/view/1

所以只需要遍历ID就行了

### 链接爬虫

可以用正则匹配自己需要的网页，可以用[urlparse](http://clouduan.tk/2017/07/27/python%E6%A8%A1%E5%9D%97urlparse/)函数进行相对链接到绝对链接的转换，可以给抓取的网址弄个set防止陷入循环链接：

```
def link_crawler(seed_url,link_regex):
    crawler_queue=[seed_url]
    seen=set(crawler_queue)
    while crawler_queue:
        url=crawler_queue.pop()
        html=download(url)
        for link in get_links(html):
            if re.match(link_regex,link):
                link=urllib.parse.urljoin(seed_url,link)
                if link not in seen:
                    seen.add(link)
                    crawler_queue.append(link)

def get_links(html):
    webpage_regex=re.compile('<a[^>]+href=["\'](.*?)["\']',re.IGNORECASE)
    return webpage_regex.findall(html)

url='http://example.webscraping.com'
link_crawler(url,'.*/(index|view)')
```

### 高级功能

解析robots.txt：robotparser模块

支持代理：urllib的代理实现：

```
import urllib.request
import urllib.parse

proxy=...  #给出proxy
opener=urllib.request.build_opener() #构造opener
proxy_params={urllib.parse.urlparse(url).scheme:proxy} #构造代理参数
opener.add_handler(urllib.request.ProxyHandler(proxy_paramsa)) #把代理参数加到opener上
response=opener.open(request)  利用opener访问
```

把代理加到download函数中：

```
def download(url,num_retries=2,proxy=None):
    print('downloading,',url)
    request=urllib.request.Request(url)

    opener=urllib.request.build_opener()
    if proxy:
        proxy_params={urllib.parse.urlparse(url).scheme:proxy}
        opener.add_handler(urllib.request.ProxyHandler(proxy_params))
    try:
        html=opener.open(request).read().decode('utf8')
    except urllib.request.URLError as e:
        print('downloading error:',e.reason)
        html=None
        if num_retries>0:
            if hasattr(e,'code') and 500<=e.code<600:
                return download(url,num_retries-1,proxy)
    return html
```

### 下载限速

可以在每次下载前调用该爬虫来频

```
import time
import datetime

class Throttle:
    def __init__(self,delay):
        self.delay=delay
        self.domain={}
        
    def wait(self,url):
        domain=urllib.parse.urlparse(url).netloc
        last_accessed=self.domain.get(domain)
        
        if self.delay>0 and last_accessed is not None:
            sleep_secs=self.delay-(datetime.datetime.now()-last_accessed).seconds
            if sleep_secs>0:
                time.sleep(sleep_secs)
        self.domain[domain]=datetime.datetime.now()

throttle=Throttle(delay)
throttle.wait(delay)
result=download(url,proxy=proxy,num_retries=num_retries)
```

### 避免爬虫死循环

避免无穷的链接，尤其是针对动态网站，需要把seen由集合变为一个字典，记录爬取网页的深度，改进的link_crawler函数：

```
def link_crawler(seed_url,link_regex):
    max_depth=2
    seen={}
    crawler_queue=[seed_url]
    while crawler_queue:
        url=crawler_queue.pop()
        html=download(url)
        depth=seen[url]
        if depth!=max_depth:
            for link in get_links(html):
                if re.match(link_regex, link):
                    link = urllib.parse.urljoin(seed_url, link)
                    if link not in seen:
                        seen[link]=depth+1
                        crawler_queue.append(link)
```

