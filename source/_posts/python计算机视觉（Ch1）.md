---
title: python图像处理的基本操作
date: 2017-07-25 11:36:42
tags: 
- python
- CV
categories: 
- 《python计算机视觉编程》学习笔记
---
# 1.1 PIL基本操作

```python
from PIL import Image
img=Image.open('./test.jpg')

a=img.resize((50,50))
b=img.rotate(45)
img.show()
a.show()
b.show()
```
<!--more-->
上面两个函数其实是另外又生成了一个对象，并非在原来的对象上进行函数操作，因此想要得到处理后的的图像，必须赋值给其它变量。经过这个处理之后，原对象`img`并没有变。  

但是下面的`thumbnail`函数却不同，原对象`img`经过该函数作用后发生了变化，该函数是在对原对象操作,所以不用再赋值操作。事实上，赋值的话会报错。

```python
img.thumbnail((50,50))
img.show()
```

为了更深刻理解，我在idle中进行了如上操作  

```
>>> img
<PIL.JpegImagePlugin.JpegImageFile image mode=RGB size=400x400 at 0x39B3810>
>>> img.resize((100,100))   
<PIL.Image.Image image mode=RGB size=100x100 at 0x39B3DF0>   
>>> img.rotate(45)
<PIL.Image.Image image mode=RGB size=400x400 at 0x39B3D50>   
```

由上可以看出，`img`,`img.resize()`,`img.rotate()`返回的的内存地址都不一样，说明是新对象。

```
>>> img.thumbnail((50,50))
>>> type(img.thumbnail((50,50)))
<type 'NoneType'>
>>> img
<PIL.JpegImagePlugin.JpegImageFile image mode=RGB size=35x50 at 0x39B3810>
```

但是由`thumbnail`这个函数作用后没有返回值，他只是一个操作而已，可以看到，`thumbnail`之后的`type`为`None`(所以赋值肯定会出错的)，而且内存地址和之前一样，不过size变成了35x50

# 1.2 Matplotlib基本操作

## 1.2.1 绘制图像，点，线

```python
from PIL import Image
from pylab import *
# 读取图像到数组中
im=array(Image.open('./test.jpg'))

#绘制图像,imshow<--pyllab
imshow(im)

#需要绘制的点
x=[100,100,400,400]
y=[200,500,200,500]

#标记点
plot(x,y,'r*')

# 绘制线
plot(x,y)

title('plot of test.jpg')
axis('off')

show()
```

im是一个378x543的矩阵（该图像的size为378x543），矩阵的每个元素为一个三元列表（记录着颜色信息）。`imshow()`开启绘图（这里没加figure因为画一张图不用担心被覆盖，pylab自动会加上窗口）。x和y为需要绘制的点，他们是一一对应的（即(100,200),(100,500),(400,200),(400,500)）。两个`plot()`函数其实是一样的，绘点还是绘线取决于最后的参数，只有线型命令就画线，只有标记命令就画点，两者都有都画，两者都没画蓝色实线（见下表：）  

| plot函数          | 说明          |
| --------------- | :---------- |
| plot(x,y)       | 默认蓝色，实线     |
| plot(x,y,'r*')  | 红色星状标记（无线）  |
| plot(x,y,'go-') | 带圆圈标记的绿线    |
| plot(x,y,'ks:') | 带正方形标记的黑色点线 |

| 颜色参数 | 说明   |
| ---- | ---- |
| 'b'  | 蓝色   |
| 'g'  |      |
| 'r'  |      |
| 'c'  | 青色   |
| 'm'  | 品红   |
| 'y'  |      |
| 'k'  | 黑色   |
| 'w'  | 白    |

| 线型参数 | 说明   |
| ---- | ---- |
| '-'  | 实线   |
| '--' | 虚线   |
| ':'  | 点线   |

| 标记参数 | 说明   |
| ---- | ---- |
| '.'  | 点    |
| '*'  | 星形   |
| '+'  | 加号   |
| 'x'  | 叉号   |
| 'o'  | 圆圈   |
| 's'  | 正方形  |

## 1.2.2 图像轮廓，直方图

先科普一下直方图：

- 直方图是图像中像素强度分布的图形表达方式.
- 它统计了每一个强度值所具有的像素个数.

用横轴代表0-255的亮度数值。竖轴代表照片中对应亮度的像素数量，这个函数图像就被称为直方图。直方图中柱子的高度，代表了画面中有多少像素是那个亮度，其实就可以看出来画面中亮度的分布和比例。

对于灰度图像，只有一种强度，从0-255,；而对于彩色图像，可想而知就有RGB三种通道的强度分布了。

下图展示了R（红）、G（绿）、B（蓝）每个通道的直方图，以及最上面叠加后的RGB直方图。

{% asset_img 直方图.jpg 直方图 %}

```Python
from PIL import Image
from pylab import *

im=array(Image.open('./test.jpg').convert('L'))

#开一个窗口
figure()
# 不使用颜色信息
gray()
#在原点的左上角显示轮廓图像
contour(im,origin='image')
axis('equal')
axis('off')

#又开一个窗口
figure()
hist(im.flatten(),128)

show()
```

此处的`im`相对于1.2.1中的`im`来说少了一维，其原因是将每个像素的颜色变成了一种，所以之前是960x635x3，这里是960x635.（看下面的代码）

```Python
#原图
>>> im=array(Image.open('./test.jpg'))
>>> im   #三个[]
array([[[ 54,  83,  79],
        [ 54,  83,  79],
        [ 54,  83,  79],
        ..., 
        [ 51,  86,  80],
        [ 53,  88,  82],
        [ 55,  90,  84]],
       
       [[118,  97,  78],
        [122, 101,  82],
        [127, 106,  87],
        ..., 
        [174, 174, 174],
        [189, 177, 177],
        [138, 118, 117]]], dtype=uint8)

>>> im.shape
(960, 635, 3)

>>> len(im)
960
>>> len(im[0])
635
>>> len(im[0][0])
3

#灰度图
>>> im
array([[ 73,  73,  73, ...,  74,  76,  78],
       [ 67,  67,  68, ...,  76,  77,  78],
       [ 68,  69,  70, ...,  71,  72,  73],
       ..., 
       [107, 103,  98, ..., 179, 185, 137],
       [101, 100,  99, ..., 189, 194, 140],
       [101, 105, 110, ..., 174, 180, 123]], dtype=uint8)
>>> im.shape
(960, 635)

>>> len(im)
960
>>> len(im[0])
635
>>> len(im[0][0])  #由此可见，只有两层
Traceback (most recent call last):
  File "<pyshell#42>", line 1, in <module>
    len(im[0][0])
TypeError: object of type 'numpy.uint8' has no len()
```

这里用了两个`figure()`函数开了两个窗口，不然作图后，后面的会盖住前面的图。`contour()`和`hist()`是两个绘图函数。第一个是用`contour()`绘制灰度图像，第二个是用`hist()`函数绘制直方图用来表征像素值的分布情况（`hist`本意就是直方图的意思），`hist()`函数接受两个参数：第二个参数是指定小区间的数目，第一个参数是一个一维数组，由于`im`是二维的，所以`flatten()`方法按照行优先的原则将其转换成一维数组。`hist()`返回一个三元列表，分别是纵轴坐标、横轴坐标、还有一段说明数据类型的字符串。

关于`flatten()`函数是如何将三维转为一维，可以参考下面的代码，可以看到其转换方式挺粗暴的(这里就可理解为拉平吧)：

```python
>>> im
array([[ 73,  73,  73, ...,  74,  76,  78],
       [ 67,  67,  68, ...,  76,  77,  78],
       [ 68,  69,  70, ...,  71,  72,  73],
       ..., 
       [107, 103,  98, ..., 179, 185, 137],
       [101, 100,  99, ..., 189, 194, 140],
       [101, 105, 110, ..., 174, 180, 123]], dtype=uint8)
>>> im.flatten()
array([ 73,  73,  73, ..., 174, 180, 123], dtype=uint8)
```



至于如何将三元列表变成一个数字，方法很多：

| 灰度算法   | 公式                            |
| ------ | ----------------------------- |
| 1.浮点算法 | Gray=R\*0.3+G\*0.59+B\*0.11   |
| 2.整数方法 | Gray=(R\*30+G\*59+B\*11)/100  |
| 3.移位方法 | Gray =(R\*76+G\*151+B\*28)>>8 |
| 4.平均值法 | Gray=(R+G+B)/3                |
| 5.仅取绿色 | Gray=G                        |

## 1.2.3 交互式标注

```Python
from PIL import Image
from pylab import *

im=array(Image.open('./test.jpg'))
imshow(im)
print ('pls clk 4 points')
x=ginput(4)
print ('you clkd'),x
show()
```

上面程序最终会在一个列表里保存这四个点的坐标。



# 1.3 NumPy

## 1.3.1 图像数组表示

```python
>>> im=array(Image.open('./test.jpg'))
>>> im.shape
(960, 635, 3)
>>> im.dtype
dtype('uint8')

>>> im=array(Image.open('./test.jpg'),'f')
>>> im.shape
(960, 635, 3)
>>> im.dtype
dtype('float32')

>>> im=array(Image.open('./test.jpg').convert('L'),'f')
>>> im.shape
(960, 635)
>>> im.dtype
dtype('float32')
>>> im[0,0,0]
```

`im.shape`返回一个元组表示图像数组的大小（行、列、颜色通道），`im.dtype`返回字符串表示数组元素的数据类型（`numpy`数组中所有元素都必须是同一数据类型）。因为图像通常被编码成无符号8位数（`uint8`），所以第一个是`uint8`，如果使用额外参数'f'，该参数会把数据类型转换为浮点型。

位于坐标i、j，颜色通道k的像素值可以这样访问：

```python
value=im[i,j,k]
#eg:
>>> im[0,0]
array([ 54.,  83.,  79.], dtype=float32)
>>> im[1,1,0]
48.0
>>> im[0] #行优先
array([[ 54.,  83.,  79.],
       [ 54.,  83.,  79.],
       [ 54.,  83.,  79.],
       ..., 
       [ 51.,  86.,  80.],
       [ 53.,  88.,  82.],
       [ 55.,  90.,  84.]], dtype=float32)
```

其它的一些操作（注意行优先原则）：

| 操作                     | 说明          |
| ---------------------- | ----------- |
| `im[i,:]=im[j,:]`      | 第j行的值赋给第i行  |
| `im[i,:]=100`          |             |
| `im[:100,:50].sum()`   | 前100行，50列的和 |
| `im[50:100,50:100]`    |             |
| `im[i].mean()`         | 第i行的平均数     |
| `im[:-1]`              |             |
| `im[-2,:] (or im[-2])` | 倒数第二行       |

## 1.3.2 灰度变换

```python
from PIL import Image
from numpy import *
from pylab import *

im=array(Image.open('./test.jpg').convert('L')) #先进行灰度变换，使得一个像素点只有一个值，方便控制
im2=255-im  #反相处理
im3=(200-100)/255*im+100  #像素值变换到100-200之间
im4=255*(im/255)**2   #二次变换，能够使得较暗的像素值变得更小。

figure()
imshow(im)
figure()
imshow(im2)
figure()
imshow(im3)
figure()
imshow(im4)

show()  #即可看到变换后的图像
```

这儿有两个关于二次变换的问题：为什么二次变换的形式是那样？为什么二次变换能让较暗的更暗？

其一，二次变换的形式是由两个条件决定的，即不论中间取什么值，但是二次变换的结果必须在0-255之间；

其二，参考下图，可以看到当取相同的x时，二次函数的y取值总是低于y=x的取值，即可说明。





{% asset_img 灰度变换.png 灰度变换 %}



如果提取的像素并没有经过灰度变换，而是3个颜色通道(下面代码)：

```python
from PIL import Image
from numpy import *
from pylab import *

im=array(Image.open('./test.jpg')) #没有灰度变换
im2=255-im  #反相处理
im3=(200-100)/255*im+100  #像素值变换到100-200之间
im4=255*(im/255)**2   #二次变换，能够使得较暗的像素值变得更小。

figure()
imshow(im)
figure()
imshow(im2)
figure()
imshow(im3)
figure()
imshow(im4)

show()  #即可看到变换后的图像
```

那么得出的图像为（以im4为例）：
{% asset_img 浮点数像素.png 浮点数像素 %}

这是为什么呢？是因为我在`from PIL import Image`这一行代码中把im4由`uint8`数据类型转变为了float数据类型，而对于三个颜色通道来说就会出现颜色混乱，结果就成了上面这样。但是对于灰度图像由于其没有颜色信息，所以没影响。

解决办法就是将在`imshow()`之前把数据类型转换回来，用`fromarray`函数：

```python
im4=Image.fromarray(uint8(im4))
```

`fromarray`函数可以把矩阵对象转换过来，变成Image对象，可以说是和`array()`相反的操作：

```python
>>> a=Image.fromarray(im)
>>> a
<PIL.Image.Image image mode=L size=635x960 at 0xCC015D0>
>>> a.size
(635, 960)
```



## 1.3.3 图像缩放

```python
def imresize(im,sz):
	pil_im=Image.fromarray(uint8(im))
	return array(pil_im.resize(sz))
```

## 1.3.4 直方图均衡化

**直方图均衡化**是图像处理领域中利用图像直方图对对比度进行调整的方法，这种方法通常用来增加许多图像的全局对比度。均衡化要用到图像值中的累积分布函数，简写cdf。关于直方图均衡化更多知识参考https://zh.wikipedia.org/wiki/%E7%9B%B4%E6%96%B9%E5%9B%BE%E5%9D%87%E8%A1%A1%E5%8C%96

```python
def histeq(im,nbr_bins=256): #nbr_bins横轴区间数目
    """针对灰度图像进行直方图均衡化"""
    
#将图像矩阵转化成直方图数据，返回元组(频数，直方图区间坐标)  imhist,bins=histogram(im.flatten(),nbr_bins,normed=True)
    cdf=imhist.cumsum()  #累积分布函数
    cdf=255*cdf/cdf[-1]  #将累积函数转化到区间[0,255]
    
    #使用累积分布函数的线性插值，扫描图像矩阵计算新的像素值（插值？）
    im2=imterp(im.flatten(),bins[:-1],cdf)
    
    #按照im的格式返回新的图像矩阵im2，以及累积分布函数
    return im2.reshape(im.shape),cdf

im=array(Image.open('./test.jpg').convert("L"))
im2,cdf=histeq(im)

#开始绘图
subplot(2,3,1)
imshow(im)             #原图像
subplot(2,3,2)
plot(cdf,range(256))   #累积分布函数图像
subplot(2,3,3)
imshow(im2)            #均衡化的图像
subplot(2,3,4)
hist(im.flatten(),256) #原图像的直方图
subplot(2,3,6)
hist(im2.flatten(),256)#均衡化图像的直方图

show()
```

上面代码中，传入原图像和区间数目，首先计算图像的直方图，`imhist`和`bins`或取的分别是强度值和区间坐标，`histogram()`和之前的`hist()`函数功能其实一样，只不过前者返回两个对象，后者返回一个列表（该列表的前两个元素就是`histogram()`返回的两个对象，下面代码可以说清楚）：

```python
>>> imhist1,cdf1=hist(im.flatten(),256)[0],hist(im.flatten(),256)[1]
>>> imhist2,cdf2=histogram(im.flatten(),256)
>>> imhist2==imhist1
array([ True,  True, ...,  True], dtype=bool)
>>> cdf1==cdf2
array([ True,  True, ...  ,True], dtype=bool)
```

累积函数有个特点就是单调递增，所以最后一个cdf[-1]肯定是最大的，如果不进行归一化，那么cdf远远超过255，所以必须进行归一化，参考下面代码和曲线图的纵坐标：

```python
cdf=imhist1.cumsum()
cdf1=255*cdf/cdf[-1]

subplot(1,2,1)
plot(range(256),cdf)

subplot(1,2,2)
plot(range(256),cdf1)

show()
```

{% asset_img 累积分布函数归一化前后对比.png 累积分布函数归一化前后对比 %}

`im2.reshape()`的意思：在此之前已经得到了im2的数据，`reshape()`就是决定如何显示im2的数据，`im2.reshape(im.shape)`的意思就是按照im的格式显示im2，im的格式是什么样的呢？

```
>>> im.shape
(200, 240)
```

所以就是让`im2`按照200x240的格式返回，那么不以这个格式可不可以呢？可以的，看代码：

```
>>> im.reshape((400,120))
array([[ 32,  32,  32, ..., 184, 184, 185],
       [184, 184, 183, ..., 222, 222, 222],
       [ 36,  35,  35, ..., 183, 183, 185],
       ..., 
       [207, 208, 210, ..., 238, 237, 238],
       [209, 210, 210, ..., 206, 207, 207],
       [207, 209, 210, ..., 237, 237, 238]], dtype=uint8)
```

也就是说`reshape()`函数接收一个元组，只要元组的两个元素的乘积一样就可以变换格式。

线性插值不是太理解，可以参考：http://blog.csdn.net/jiaqiangbandongg/article/details/54694632 

最后绘图显示前后的效果。**bingo~** 

## 1.3.5 图像平均

图像平均是减噪的一种方式，常用于艺术特效。下面代码是将图像的矩阵简单地相加相除得到平均图像。

注意：待处理的图像必须要相同大小，这样才有对应矩阵行列相同，才可以相加。

```python
def compute_average(imlist):
    
    #打开第一个图像，将其存储在浮点型数组中
	averageim=array(Image.open(imlist[0],'f'))
    #遍历其余的图像
	for imname in imlist[1:]:
		try:
			averageim+=array(Image.open(imname))
		except:
			print(imname+'...skiped')
	averageim/=len(imlist)
    #返回uint8类型的平均图像
	return array(averageim,'uint8')
```

## 1.3.6 图像的主成分分析（PCA）

PCA是常用的一个降维技巧，需要目标一点，图像的维度其实是指像素点的数量，比如一幅100x100像素的灰度图像，其维数就是10000维。

代码运行会报错，但是最后还是可以画出图像，整个研究了一天，PCA部分没研究。研究时参考[Numpy笔记](./NumPy数组.md)

```python
from numpy import *
from pylab import *
from PIL import Image

def pca(X):
	#获取维数
	num_data,dim=X.shape
	#数据中心化
	mean_X=X.mean(axis=0)
	X=X-mean_X
	
	if dim>num_data:
        #使用紧致技巧，不用svd
		M=dot(X,X.T)  #协方差矩阵
		ee,EV=linalg.eigh(M) #特征值和特征向量
		tmp=dot(X.T,EV).T
		V=tmp[::-1]
		print(ee)
		S=sqrt(ee)[::-1]
		for i in range(V.shape[1]):
			V[:,i] /= S
	else:
        #使用svd方法
		U,S,V=linalg.svd(X)
		V=V[:num_data]
	return V,S,mean_X

imlist=['image_0001.jpg','image_0002.jpg','image_0003.jpg','image_0004.jpg','image_0005.jpg','image_0006.jpg']
im=array(Image.open('c://Python36/sample/'+imlist[0]))
m,n=im.shape[0:2]
imnbr=len(imlist)

#创建矩阵，将每一幅图片数据拉成一维，然后再用array()函数将其组合在一起，
immatrix=array([array(Image.open('c://Python36/sample/'+im)).flatten() for im in imlist],'f')

#PCA操作
V,S,immean=pca(immatrix)

figure()
gray()
subplot(2,4,1)
imshow(immean.reshape(m,len(immean)//m))  #均值图像
for i in range(6):
    subplot(2,4,i+2)
    imshow(V[i].reshape(m,len(immean)//m))

show()
```

## 1.3.7 使用pickle模块--序列化

序列化：变量从内存中变成可存储或传输的过程称之为序列化，他可以直接把程序中的变量或数据结构写入文件，然后可以直接从文件中原样读成变量或数据结构（这个过程叫反序列化）。

而像`file`函数操作只是存储和读取字符串格式的数据，pickle可以存储和读取成其他格式比如list dict的数据。

+ dump()和load()

  `pickle.dump()`直接把对象序列化后写入一个`file-like Object`；

  `pickle.load()`方法从一个`file-like Object`中直接反序列化出对象

  ```python
  import pickle
  #写入
  >>> d=dict(name='xiaoming',age=14)
  >>> l=['xiaoming','xiaopang']
  >>> f=open('mess.pkl','wb')
  >>> pickle.dump(d,f) #写入d
  >>> pickle.dump(l,f) #写入l
  >>> f.close()
  #读取
  >>> f=open('mess.pkl','rb')
  >>> D=pickle.load(f) #读取d的数据赋给D
  >>> L=pickle.load(f) #读取l的数据赋给L
  >>> D
  {'name': 'xiaoming', 'age': 14}
  >>> L
  ['xiaoming', 'xiaopang']
  >>> f.close()
  ```

  需要注意的是以上读取的顺序必须和写入的顺序一致，先写入的先读取。

+ loads()和dumps()

  顾名思义，loads-->load string ; dumps --> dump string 就是把对象转化为bytes。其后可以用write()方法进行写入。

  ```python
  >>> d=dict(name='xiaoming',age=14)
  >>> tmp=pickle.dumps(d)
  >>> tmp
  b'\x80\x03}q\x00(X\x04\x00\x00\x00nameq\x01X\x08\x00\x00\x00xiaomingq\x02X\x03\x00\x00\x00ageq\x03K\x0eu.'
  >>> D=pickle.loads(tmp)
  >>> D
  {'name': 'xiaoming', 'age': 14}
  #写入
  >>> with open('mess.pkl','wb') as f:
  	f.write(tmp)
  	
  46
  ```

所以以上者两组的联系就是：dumps+write==dump；read+loads==loads

此外，json也是一种序列化的实现方法，JSON表示出来就是一个字符串，可以被所有语言读取，也可以直接在web页面中读取。

JSON表示的对象就是标准的JavaScript语言的对象，JSON和Python内置的数据类型对应如下：

| JSON类型     | Python类型   |
| ---------- | ---------- |
| {}         | dict       |
| []         | list       |
| "string"   | str        |
| 1234.56    | int或float  |
| true/false | True/False |
| null       | None       |

python内置json模块，其基本操作函数和pickle一样。

参考：https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000

# 1.4 Scipy

## 1.4.1图像模糊--高斯模糊

+ 数学原理：

  以灰度图像为例，把图像的每个像素的灰度值，变换为它周围邻近的N个像素值的平均值，得出的图像就有了模糊效果，但这种效果不理想，体现不出边缘（不够自然），所以提出了一种比较理想的方法，就是使用*加权平均值*，因为对某个像素而言，离它越近的像素，与它的关联性越高，所以权值应该越大，相反，离它越远的像素，与它的关联性越低，权值应该越小。

  那分配权值应该使用什么样的算法呢？最常用的就是*高斯分布函数*。

  ![img](https://segmentfault.com/img/bVqZUc)

  上面这个称作高斯核。如果我们给定一个sigma值以及高斯分布的大小（范围），就能得到一个矩阵，即权值矩阵（又叫高斯平均算子）。比如这个：

  ![img](https://segmentfault.com/img/bVq3Rk)

  以此矩阵为卷积模板，和原图像进行卷积操作，就可以实现图像的高斯模糊。

  所以本质上，图像模糊就是将（灰度）图像**I**和一个高斯核进行卷积操作：
  $$
  I(alpha)=I * G(alpha)
  $$







其中，G(alpha)为标准差为alpa的二维高斯核，定义为：
$$
G(alpha)=1/(2pialpha^2)*e^-(x^2+y^2)/2alpha^2
$$
alpha的值越大，图像被模糊得越厉害。

+ 模糊一幅灰度图像：

  ```python
  from PIL import Image
  from numpy import *
  from pylab import *
  from scipy.ndimage import filters

  im=array(Image.open('./test.jpg').convert('L'))
  im2=filters.gaussian_filter(im,3)  #alpha=3

  figure()
  imshow(im2)
  gray()
  show()
  ```

+ 模糊一幅彩色图像：

  ```python
  from PIL import Image
  from scipy.ndimage import filters
  import numpy as np
  import matplotlib.pyplot as plt

  im=np.array(Image.open('c://Python36/test.jpg'))
  index=221
  plt.subplot(index)
  plt.imshow(im)

  for sigma in (2,5,8):
      im2=np.zeros(im.shape,dtype=np.uint8)
      for i in range(3):
          im2[:,:,i]=filters.gaussian_filter(im[:,:,i],sigma)

      index+=1
      plt.subplot(index)
      plt.imshow(im2)

  plt.show()

  ```

  reference:https://segmentfault.com/a/1190000004002685

## 1.4.2 图像导数--梯度

+ 图像导数：图像处理中，图像强度的变化可以用灰度图像I（对于彩色图像通常对每个颜色通道分别计算导数）的x方向的导数Ix和y方向的导数Iy进行描述。

  由于图像是一个离散空间，无法求真正的导数，只能通过多项式拟合---图像中某一点的导数，就是该点在x方向和y方向的变化率。

  > **dx(i,j)  = I(i+1,j) - I(i,j)**  #x方向偏导，近似为某行与其上一行的差值
  >
  > **dy(i,j)  = I(i,j+1) - I(i,j)** #y方向偏导，近似为某列与其上一列的差值

+ 图像梯度：仿照函数梯度的定义，图像函数f(x,y)在点(x,y)的梯度是一个具有大小和方向的矢量，设Gx 和 Gy 分别表示x方向和y方向的导数（其实就是上面的dx，dy），这个梯度的矢量可以表示为：

  ![img](http://img.blog.csdn.net/20140316165211125)

  有两个重要属性：

  1.梯度的大小：描述了图像强度变化的强弱

  ![img](http://img.blog.csdn.net/20140316165304000)

  2.梯度的方向：描述了图像在每个点（像素）上强度变化最大的方向。

  ![img](http://img.blog.csdn.net/20140316165546156)

+ 梯度算子：经典的图像梯度算法是考虑图像的每个像素的某个邻域内的灰度变化，利用边缘临近的一阶或二阶导数变化规律，对原始图像中像素某个邻域设置梯度算子，通常我们用小区域模板进行卷积来计算，有Sobel算子、Robinson算子、Laplace算子等

  于是上面的
  $$
  Gx=I*Dx
  $$

  $$
  Gy=I*Dy
  $$

  其中Dx和Dy就是滤波器（以Sobel滤波器为例）：

  ![img](https://saush.files.wordpress.com/2011/04/filters.png)

  对于滤波器的使用原理（就是进行卷积操作），以Roberts算子为例进行图示：

  ![img](http://my.csdn.net/uploads/201205/13/1336886163_8877.png)

+ 代码实现：

  ```python
  from PIL import Image
  from numpy import *
  from pylab import *
  from scipy.ndimage import filters

  im=array(Image.open('./test.jpg').convert('L'))

  #使用sobel导数滤波器
  imx=zeros(im.shape)
  filters.sobel(im,1,imx)
  imy=zeros(im.shape)
  filters.sobel(im,0,imy)

  mag=sqrt(ixm**2+imy**2)

  imshow(mag) #imshow(imx) #imshow(imy)
  ```

  上述代码中，0和1表示选择y或x方向的导数，其中1是x方向，0是y方向。`imx`或`imy`表示用以输出的变量。最终的导数图像（imshow(imx)和imshow(imy)），正导数显示为亮的像素，负的显示为暗的，灰色区域表示导数的值接近为零。

  但是有一些缺陷：滤波器的尺寸需要随着图像分辨率的变化而变化。为了在任意尺度上计算导数，我们使用高斯导数滤波器：
  $$
  Ix=I*Gax
  $$

  $$
  Iy=I*Gax
  $$

  其中G表示在x和y方向上的导数，Ga指标准差为a的高斯函数。

  ```python
  sigma=5  # 标准差

  imx=zeros(im.shape)
  filters.gaussian_filter(im,(sigma,sigma),(0,1),imx)

  imy=zeros(im.shape)
  filters.gaussian_filter(im,(sigma,sigma),(1,0),imy)

  mag=(imx**2+imy**2)
  ```

  (0,1)和(1,0)参数指定对每个方向计算哪种类型的导数。（**?**）


梯度图像可以有效获取图像的轮廓。


reference：

http://blog.csdn.net/computer_liuyun/article/details/21328753

http://www.jianshu.com/p/f0429f684f32

http://math.fudan.edu.cn/gdsx/KEJIAN/%E6%96%B9%E5%90%91%E5%AF%BC%E6%95%B0%E5%92%8C%E6%A2%AF%E5%BA%A6.pdf

http://blog.csdn.net/jia20003/article/details/7562092

## 1.4.3 形态学：对象计数

## 1.4.4 有用的SciPy模块--io&misc

+ io---读取.mat文件

  ```python
  import scipy.io
  #保存字典为mat数组形式
  >>> data=dict(name='xiaoming',age=90)
  >>> scipy.io.savemat('test.mat',data)
  #载入
  >>> data1=scipy.io.loadmat('test.mat')
  >>> data1
  {'__header__': b'MATLAB 5.0 MAT-file Platform: nt, Created on: Sun Jul 23 11:47:48 2017', '__version__': '1.0', '__globals__': [], 'name': array(['xiaoming'],
        dtype='<U8'), 'age': array([[90]])}
  ```

+ misc--以图像形式保存数组

  ```python
  >>> import scipy.misc
  >>> im=np.array(Image.open('./test.jpg'))
  >>> scipy.misc.imsave('im.jpg',im) #把im数组保存在了im.jpg中
  ```

  misc模块还可以调出内置的测试图像：

  ```python
  >>> face=scipy.misc.face()
  >>> plt.imshow(face)
  <matplotlib.image.AxesImage object at 0x01935890>
  >>> plt.show()

  >>> ascent=scipy.misc.ascent()
  >>> plt.imshow(ascent)
  <matplotlib.image.AxesImage object at 0x018CD610>
  >>> show()
  ```

  还可以查看当前环境下内存中的图像缓存文件：

  ```python
  >>> face=scipy.misc.who()
  Name            Shape                Bytes            Type
  ===============================================================

  im              200 x 240 x 3        144000           uint8
  im2             200 x 240 x 3        144000           uint8
  lena            512 x 512            1048576          int32
  face            768 x 1024 x 3       2359296          uint8

  Upper bound on total bytes  =       3695872
  ```

  ​


## 1.4.5 图像去噪

利用**变分法**的ROF图像去噪实现

rof.py

```
import numpy as np

def denoise(im,U_init,tolerance=0.1,tau=0.125,tv_weight=100):
    m,n=im.shape

    U=U_init
    Px=im
    Py=im
    error=1

    while error>tolerance:
        Uold=U
        GradUx=np.roll(U,-1,axis=1)-U
        GradUy=np.roll(U,-1,axis=0)-U

        PxNew=Px+(tau//tv_weight)*GradUx
        PyNew=Py+(tau//tv_weight)*GradUy
        NormNew=np.maximum(1,np.sqrt(PxNew**2+PyNew**2))

        Px=PxNew//NormNew
        Py=PyNew//NormNew

        RxPx=np.roll(Px,1,axis=1)
        RyPy=np.roll(Py,1,axis=0)

        DivP=(Px-RxPx)+(Py-RyPy)
        U=im+tv_weight*DivP

        error=np.linalg.norm(U-Uold)/np.sqrt(n*m)

        return U,im-U
```

main1.py

```
import numpy as np
from pylab import *
import rof

im=np.zeros((500,500))
im[100:400,100:400]=128
im[200:300,200:300]=255
im+=30*np.random.standard_normal((500,500))  #制造噪声

U,T=rof.denoise(im,im)

figure()
gray()
subplot(1,2,1)
imshow(im)
subplot(1,2,2)
imshow(U)
show()
```

示例效果图

{% asset_img 图像去噪1.png %}