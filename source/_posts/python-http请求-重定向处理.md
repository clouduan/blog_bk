---
title: python-Http请求中的重定向处理
date: 2017-09-03 14:53:57
tags:
- urllib
- requests
- 重定向
- cookies
---

在模拟登录时，重定向也一直是比较难搞的东西。对于Python来说有urllib和requests模块，本文记录两者对重定向的处理方法。

<!--more-->

## requests

只需要在请求方法里加参数指明是否禁用重定向：

```
requests.get(url,allow_redirects=False)
```

如果想要保存cookies，则使用`requests.session()`即可。

## urllib.request

有自带`HTTPRedirectHandler`类，其大概结构是：

```
class HTTPRedirectHandler(BaseHandler):
    # 在同一domain的重定向次数
    max_repeats = 4
    # 总共的重定向次数
    max_redirections = 10

    def redirect_request(self, req, fp, code, msg, headers, newurl):
        """
        接受新的url，得到请求方式（post/get），并构造新的请求（Request）。
        """
        ...
    def http_error_302(self, req, fp, code, msg, headers):
        """
        从headers得到location对应的值（即为目标url），然后调用self.redirect_request方法，并循环检测重定向。
        """
        ...
    #30x都作302处理
    http_error_301 = http_error_303 = http_error_307 = http_error_302
```

这个类将自动跟从重定向，然而很多时候我的需求多样：比如我只想要cookies而不想跟从重定向；再比如有的`headers`里面的`location`是相对路径，要避免代码无法识别而报错。此时候我就需要通过继承这个类进行自定义，可以重写类方法，也可以自己另外定义新方法。最在构建`opener`时安装即可。

### 例：比如我不跟从重定向：

```
class MyHTTPRedirectHandler(urllib.request.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        return urllib2.HTTPRedirectHandler.http_error_302(self, req, fp, code, msg, headers)
    http_error_301 = http_error_303 = http_error_307 = http_error_302

redirect_handler = MyHTTPRedirectHandler()
opener=urllib.request.build_opener(redirect_handler)
urllib.request.install_opener(opener)
```

现在我要保存cookies，有两种方案：

1. 从服务器返回的response headers中抽取`Set-Cookie`，然后手动添加到request headers：

   ```
   class MyHTTPRedirectHandler(urllib.request.HTTPRedirectHandler):
        
        @staticmethod
       def __cookie_handler(headers):  
            for msg in headers:
               if msg.find('Set-Cookie:') != -1:
                   return msg.replace('Set-Cookie:', '')
               return ''
               
       def http_error_302(self, req, fp, code, msg, headers):
           cookie = self.__cookie_handler(headers) # 得到cookies
           if cookie != '':
           req.add_header("Cookie":cookie)         # 添加cookies
           return urllib2.HTTPRedirectHandler.http_error_302(self, req, fp, code, msg, headers)
       http_error_301 = http_error_303 = http_error_307 = http_error_302

   __redirect_handler = MyHTTPRedirectHandler()
   urllib.request.build_opener(MyHTTPRedirectHandler)
   urllib.request.install_opener(__req)
   ```

2. 利用cookieJar

   ```
   cookie = http.cookiejar.CookieJar()
   cookie_handler = urllib.request.HTTPCookieProcessor(cookie)
   redirect_handler = MyHTTPRedirectHandler()
   # 安装cookie_handler和 redirect_handler
   opener = urllib.request.build_opener(cookie_handler, redirect_handler)

   urllib.request.install_opener(req)
   ```

   然后在请求中会自动处理cookies

## P.S.

发现一个代码示例网站，可以看对一些类的经典使用方法：https://www.programcreek.com/

关于*HTTPRedirectHandler*类的使用示例在这 https://www.programcreek.com/python/example/1259/urllib2.HTTPRedirectHandler

https://gxnotes.com/article/108689.html  子类化+cookies设置

http://blog.sina.com.cn/s/blog_93b1f3f0010146l3.html  子类化+cookies设置
