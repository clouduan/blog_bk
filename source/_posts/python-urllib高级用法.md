---
title: python-urllib高级用法
date: 2017-09-03 19:39:00
tags:
- urllib
---

urllib是Python用于web请求的利器，属于自带的包。urllib/request.py负责请求操作，urllib/response.py负责响应操作，urllib/parse.py负责解析操作，urllib/error.py负责错误处理。本文记录了自己看urllib的一些所得，力求结构上的系统性。

<!--more-->

# opener&handler对象

opener对象是**OpenerDirector**类的实例，用来打开URL，一般通过`urlopen`方法使用的默认opener；自定义opener有`open`方法，类似于`urlopen`。

handler对象是**BaseHandler**类的实例，构建自定义opener需要使用处理器handler，不同的handler有不同的功能。以下是官方的常用handler：

HTTPHandler() 通过HTTP打开URL
CacheFTPHandler() 具有持久FTP连接的FTP处理程序
FileHandler() 打开本地文件
FTPHandler() 通过FTP打开URL
HTTPBasicAuthHandler() 通过HTTP验证处理
HTTPCookieProcessor() 处理HTTP cookie
HTTPDefaultErrorHandler() 引发HTTPError异常处理HTTP错误
HTTPDigestAuthHandler() HTTP摘要验证处理
HTTPRedirectHandler() 处理HTTP重定向
HTTPSHandler() 通过安全HTTP重定向
ProxyHandler() 通过代理重定向请求
ProxyBasicAuthHandler 基本的代理验证
ProxyDigestAuthHandler 摘要代理验证
UnknownHandler 处理所有未知URL的处理程序

## 构建自定义opener的方法：

### 实例化OpenerDirector类创建opener对象

OpenerDirector是urllib.request的一个类，所有的opener对象都是由这个类的实例。这里我直接将类实例化就创建了一个opener对象。

#### 源码如下：

```python
# urllib/request.py
class OpenerDirector:
    def __init__(self):
        client_version = "Python-urllib/%s" % __version__
        self.addheaders = [('User-agent', client_version)]
        # self.handlers is retained only for backward compatibility
        self.handlers = []
        # manage the individual handlers
        self.handle_open = {}
        self.handle_error = {}
        self.process_response = {}
        self.process_request = {}

    def add_handler(self, handler):
    def close(self):
    def _call_chain(self, chain, kind, meth_name, *args):
    def open(self, fullurl, data=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT):
    def _open(self, req, data=None):
    def error(self, proto, *args):
```

可以看到其有`self.handlers = []`属性，还有`add_handler`方法，通过该方法将handler传入；`open`方法，通过该方法访问URL。

#### 用法示例：

```
import urllib.request

opener=urllib.request.OpenerDirector()
opener.add_handler(HTTPRedirectHandler,ProxyHandler)
opener.open('http://www.clouduan.tk')
```

### 使用build_opener函数创建opener对象

`build_opener`函数返回一个opener对象，默认添加几个处理器，也可以由使用者添加或更新默认处理器。

#### 源码如下：

```
# urllib/request.py
def build_opener(*handlers):
    """Create an opener object from a list of handlers.
    The opener will use several default handlers, including support
    for HTTP, FTP and when applicable HTTPS.
    If any of the handlers passed as arguments are subclasses of the default handlers, the default handlers will not be used.
    """
    opener = OpenerDirector()
    default_classes = [ProxyHandler, UnknownHandler,HTTPHandler,HTTPDefaultErrorHandler, HTTPRedirectHandler,FTPHandler, FileHandler,HTTPErrorProcessor,DataHandler]
    ...
    return opener
```

可以看到仍旧是通过OpenerDirector类创建了一个对象，只不过加了几个默认的handler，而且函数说明中指明“用户另外添加的handler的子类优先”，所以我可以添加或更新handlers。

#### 用法示例：

可以在创建时就把handler作为参数传进去：

```
opener=urllib.request.build_opener(HTTPRedirectHandler)
```

也可以在创建opener之后再`add_handle`：

```
opener=urllib.request.build_opener()
opener.add_handler(HTTPRedirectHandler)
```

### 使用install_opener创建全局opener对象

`install_opener`必须要在`build_opener`之后，用来创建（全局）默认的opener，也就是说之后调用`urlopen`将使用我安装的opener。但是通常不这么用，因为会导致灵活性降低，一般还是作为一个自定义opener，通过调用`opener.open()`实现相关功能。

#### 源码如下：

```
def install_opener(opener):
    global _opener
    _opener = opener
```

只是全局化了`_opener`变量。要弄清楚它是如何影响`urlopen`的，我觉得还有必要看下`urlopen`的源码：

```
def urlopen(url, data=None, timeout=socket._GLOBAL_DEFAULT_TIMEOUT,*, cafile=None, capath=None, cadefault=False, context=None):
    global _opener
    if cafile or capath or cadefault:
        ...
        https_handler = HTTPSHandler(context=context)
        opener = build_opener(https_handler)
    elif context:
        https_handler = HTTPSHandler(context=context)
        opener = build_opener(https_handler)
    elif _opener is None:
        _opener = opener = build_opener()
    else:
        opener = _opener
    return opener.open(url, data, timeout)
```

可以看到它本质上是一个`opener.open()`， `install_opener`影响的只是它的opener对象。由于我们一般使用`urlopen`时参数比较简单（cafile=None, capath=None, cadefault=False, context=None），所以该函数一般只是执行后两个条件语句。

#### 用法示例：

```
opener=urllib.request.build_opener(HTTPError)
urllib.request.install_opener(opener)
urllib.request.urlopen('http://clouduan.tk')
```

## 应用

### Proxy 设置

可以使用`ProxyHandler(proxies)`，参数`proxies`是一个字典，将协议名称（http，ftp）等映射到相应代理服务器的URL。

```
proxy_handler=urllib.request.ProxyHandler({'http':'http://127.0.0.1:8087'})
opener=urllib.request.build_opener(proxy_handler)
```

网上说还可以用

```
urlopen(url,proxies={'http':'http://127.0.0.1:8580'})
```

但是我在源码中没有看到这种用法，查了Python2的urllib源码才知道这个是Python2.7urllib模块独有的方法。

### Redirect设置

见[python-http请求-重定向处理]()

### Cookie设置

见[python-http请求-cookie处理]()

# Request对象

request对象是**Request**类的实例，

## 源码大略：

```
class Request:

    def __init__(self, url, data=None, headers={},origin_req_host=None, unverifiable=False,method=None):
        self.full_url = url
        self.headers = {}
        self.unredirected_hdrs = {}
        self._data = None
        self.data = data
        self._tunnel_host = None
        for key, value in headers.items():
            self.add_header(key, value)
        if origin_req_host is None:
            origin_req_host = request_host(self)
        self.origin_req_host = origin_req_host
        self.unverifiable = unverifiable
        if method:
            self.method = method

    def ...
    def get_method(self):
    def get_full_url(self):
    def set_proxy(self, host, type):
    def has_proxy(self):
    def add_header(self, key, val):
    def add_unredirected_header(self, key, val):
    def has_header(self, header_name):
    def get_header(self, header_name, default=None):
    def remove_header(self, header_name):
    def header_items(self):
```

可以看到有很多处理headers的相关操作，也有处理cookies的操作！

## 应用

### Headers设置

`request.add_header(key,val)` key是报头名，val是包头值，两个参数都是字符串。
`request.add_unredirected_header(key,val)` 同上，但是不会添加到重定向请求中。

```
req=urllib.request.Request(url)
req.add_header('User-Agent', 'chrome...')
urllib.request.urlopen(req)
```

注意要加入headers，需要使用Request对象，该对象有def `add_header(self, key, val)`方法，要求传入键-值两个参数。

### Cookies设置

```
req=urllib.request.Request(url)
request.set_proxy('127.0.0.1:8580','http')
urllib.request.urlopen(req)
```

# response对象的常用方法

方法的源码在urllib/response.py中。

`geturl()` — 返回真正的url，通常用来鉴定是否重定向
`info()` — 返回页面的原信息就像一个字段的对象， 如headers。
`getcode()` — 返回响应的HTTP状态代码。









## References

[python的urllib2库详细使用说明](http://www.cnblogs.com/kennyhr/p/4018668.html)

[Python 标准库 urllib2 的使用细节](http://zhuoqiang.me/python-urllib2-usage.html)