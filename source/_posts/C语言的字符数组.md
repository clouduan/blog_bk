---
title: C语言的字符数组
date: 2017-08-11 00:26:32
tags:
- C语言
categories:
- C语言学习笔记
---
最近看C看到数组部分，发现C语言比Python的复杂多了，尤其是还有个字符数组更迷了，在此作个记录总结。

## 字符&字符串&字符数组

C语言中字符常亮是由一对单引号括起来的字符，字符串是由双引号括起来的字符序列，字符数组指的是每个元素都是字符型数据的数组。

C中没有专门的字符串变量，通常用一维字符数组来存放一个字符串常量，把一维字符数组称作字符串变量。
<!--more-->

## 字符数组的定义和初始化

数组初始化的形式多样，例如：

```c
char str[3];   //定义一个一维字符数组
char str[2][3];  //二维字符数组
char str[4]={'d','y','z'};  //给部分数组元素赋值
char str[5]={'c','l','o','u','d'}  //给全体元素赋值，注意长度可以为5，这种情况下没有 '\0'
char str[]={'c','l','o','u','d'}  //给全体元素赋值，可以不指定长度
char str[6]={"cloud"}   //可以将字符串直接赋值给字符数组,注意长度为6('\0'，不然会丢失'\0'
char str[]={"cloud"}   //可以省略长度
char str[]="cloud"     //这种形式更加简介
```

但是如果在定义时不进行初始化，则必须制定数组的长度：

```c
char str[];  //错误的写法
```



## 字符串的输出

### 逐个字符输出

用`%c`，结合循环语句可以输出整个字符串：

```c
void main()
{
    char str[] = "clouduan";
    for (int i = 0; i <9 ; ++i) {
        printf("%c", str[i]);
    }
}
// 输出为  clouduan
```

### 整个字符串输出

用`%s`，对应的输出项是字符串或者字符数组名，而非字符数组元素：

```c
void main()
{
    char str[] = "cloud";
    printf("%s", str);
}
//输出为  cloud
```

### 结束标志符

字符串末尾会有一个结束标志符`'\0'`，其对应的二进制位00000000，在输出时遇到该符号会停止输出

```c
void main() {
    char str[] = "cloud";
    str[3] = '\0';
    printf("%s", str);
}
//输出 clo
```

而且，字符数组本身并不要求最后要有标志 `'\0'`，但当字符数组作为字符串时就必须要有`'\0'`：

```c
void main() {
    char str1[] = {'c', 'l', 'o', 'u', 'd'};
    char str2[] = "cloud";
    printf("%s\n", str2);
    //printf("%s", str1); 这种写法错误，因为这个会在输出cloud之后继续输出，直到遇到8位'\0'为止
}
```

上面代码中，如果给数组赋值时把每个字符单独用单引号括起来（str1），会丢失`'\0'`，可以采用一下三种办法使数组以`'\0'`结束：

1. 写成str2的形式
2. 人工添加'\0': `char str1[] = {'c', 'l', 'o', 'u', 'd','\0'};`
3. 定义时预先留一个空位: `char str1[6]={'c', 'l', 'o', 'u', 'd','\0'}` 

## 字符串的输入

使用`scanf`函数进行输入，但有两点需要特别注意：

1. 输入字符串时，不能有空格，因为函数会将空格视为输入字符串的结束符：

   ```c
   void main()
   {
       char str[6];
       scanf("%s", str);
       printf("%s", str);
   }
   //输入 cloud  输出  cloud
   //输入 clo ud  输出  clo
   ```

   这种机制蛮好的，因为如果同时给几个字符串赋值就可以像下面这样：

   ```c
   void main()
   {
       char str1[6];
       char str2[6];
       char str3[6];
       scanf("%s%s%s", str1,str2,str3);
       printf("str1=%s\n str2=%s\n str3=%s", str1,str2,str3);
   }
   /* 输入  i am cloud
    输出为  str1=i
           str2=am
           str3=cloud   */
   ```

2. 一般而言，`scanf`函数的输入项必须是地址方式，如&a，&b，但是对于数组名来说并不需要这样。由于C语言中整个数组是以首地址开头的一块连续的内存单元，因此规定数组名代表该数组的首地址。以 char a[3] 为例，数组名a就代表a[0]的地址，因而不再需要加取址运算符。

## 字符串处理函数

### 字符串输入输出函数

对于字符输入输出函数有 `getchar()`和`putchar`，则相应地对于字符串有 `gets()`和`puts()`,是getstring和putstring的缩写。

puts()函数将字符串内容输出到终端并换行（会自动把字符串中的'\0'换成换行符'\n'）

gets(str)函数将输入的字符串赋给str，回车会被转换成'\0'

### 字符串求长函数

strlen(str)返回str中的**有效**字符个数，不包括'\0'：

```c
#include <stdio.h>
#include <memory.h> //strlen函数在memory.h头文件里

void main()
{
    char a[10] = "cloud";
    printf("%d", strlen(a));
}
//输出 5
```

### 字符串连接函数

stract(str1,str2)，将str2连接在str1后面，要求str1足够大以便容纳str2：

```c
#include <stdio.h>
#include <string.h> //strcat函数

void main() {
    char str1[] = "cloud";
    char str2[] = " sophie";
    strcat(str1, str2);
    printf("%s", str1);
}
//输出 cloud sophie
```

### 字符串拷贝函数

strcpy(str1,str2)把str2拷贝到str1，要求str1足够大以便容纳str2：

```c
#include <stdio.h>
#include <string.h>

void main() {
    char str1[] = "cloud";
    char str2[] = "sophie000";
    strcpy(str1, str2);
    printf("%s", str1);
}
//输出  sophie000
```

strncpy(str1,sr2,n)把str2的前n个字符拷贝到str1：

```c
#include <stdio.h>
#include <string.h>

void main() {
    char str1[] = "cloud";
    char str2[] = "sophie000";
    strncpy(str1, str2,3);
    printf("%s", str1);
}
//输出  sopud
```

### 字符串比较函数

strcmp(str1,str2)对两个字符串从左到右比较，知道出现不同的字符或遇到'\0'为止。如果全部字符相同则返回0，若出现第一个不相同的字符，则按照ascii码大小比较相应位置的字符：str1的小于str2的则返回负整数；否则返回正整数。

```c
#include <stdio.h>
#include <string.h>

void main() {
    char str1[] = "cloud";
    char str2[] = "cloud";
    char str3[] = "sophie";
    printf("%d\n", strcmp(str1,str2));  //不能用 str1==str2
    printf("%d\n", strcmp(str1,str3));
}
/*输出 
0
-16*/ 
```

### 字符串字母转换函数

strlwr(str)将str中的大写字母转换成小写字母，strupr(str)将str中的小写字母转换成大写字母。不过这俩函数只能在Windows下（VC和MinGW）使用。
