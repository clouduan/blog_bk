---
title: openface运行Demos
date: 2017-07-25 13:21:37
tags:
- face recognition
- openface
- CV
categories:
- face recognition
---
# 1.训练分类器（Training a Classifier）  


### 建立个人文件夹  

在openface根目录下新建 `clouduan`私人文件夹，存放自己的训练数据,openface requires more than one class，因此要至少放两个人的照片，分别相同数量的若干张。

<!--more-->

```  
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ tree      
.  
├── aligned_images  
│   ├── person1  
│   └── person2 
└── raw_images  
    ├── person1  
    │   ├── 1.png  
    │   ├── 2.png  
        ...  
    │   └── 9.png  
    └── person2
        ├── 1.png  
        ├── 2.png  
        ...  
        └── 9.png 
```

### 修图片  

人脸发现和矫正和裁剪 对其的新照片将放入./clouduan/aligned-images/目录中，是96*96的正方形图片：  

```
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ sudo ../util/align-dlib.py ./raw-images/ align outerEyesAndNose ./aligned-images/ --size 96
=== ./raw-images/person1/7.png ===
=== ./raw-images/person2/89.png ===
=== ./raw-images/person2/910.png ===
=== ./raw-images/person1/9.png ===
=== ./raw-images/person2/11.png ===
=== ./raw-images/person1/5.png ===
=== ./raw-images/person2/56.png ===
=== ./raw-images/person2/67.png ===
=== ./raw-images/person1/4.png ===
=== ./raw-images/person1/6.png ===
=== ./raw-images/person2/34.png ===
=== ./raw-images/person1/2.png ===
=== ./raw-images/person1/3.png ===
=== ./raw-images/person2/78.png ===
=== ./raw-images/person1/8.png ===
=== ./raw-images/person2/45.png ===
=== ./raw-images/person2/23.png ===
=== ./raw-images/person1/1.png ===
```
### 提特征

每个人脸128个特征点，新特征为csv文件，放在./clouduan/generated-embeddings/目录中：

```
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ sudo ../batch-represent/main.lua -outDir ./generated-embeddings/ -data ./aligned-images/
{
  data : "./aligned-images/"
  imgDim : 96
  model : "/home/clouduan/code/openface/models/openface/nn4.small2.v1.t7"
  device : 1
  outDir : "./generated-embeddings/"
  cache : false
  cuda : false
  batchSize : 50
}
./aligned-images/	
cache lotation: 	/home/clouduan/code/openface/clouduan/aligned-images/cache.t7	
Loading metadata from cache.	
If your dataset has changed, delete the cache file.	
nImgs: 	17	
Represent: 17/17	
```
### 训练

训练自己的面部检测模型，是最基本的SVM模型：classifier.pkl ，会放入generated-embeddings/目录下：  
```
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ sudo ../demos/classifier.py train ./generated-embeddings/
/usr/local/lib/python2.7/dist-packages/sklearn/lda.py:4: DeprecationWarning: lda.LDA has been moved to discriminant_analysis.LinearDiscriminantAnalysis in 0.17 and will be removed in 0.19
  "in 0.17 and will be removed in 0.19", DeprecationWarning)
Loading embeddings.
Training for 2 classes.
Saving classifier to './generated-embeddings//classifier.pkl'
```

### 检测  
获取一张不在数据集中的照片，利用分类器检测并归属然后会得到一个属于哪个名字的结果 和 一个可信度数值：  
新图片 为 ./clouduan/raw_images/a.png ;./clouduan/raw_images/b.png   
```
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ sudo ../demos/classifier.py infer ./generated-embeddings/classifier.pkl b.png 
/usr/local/lib/python2.7/dist-packages/sklearn/lda.py:4: DeprecationWarning: lda.LDA has been moved to discriminant_analysis.LinearDiscriminantAnalysis in 0.17 and will be removed in 0.19
  "in 0.17 and will be removed in 0.19", DeprecationWarning)

=== b.png ===
Predict person2 with 0.89 confidence.
```
```
clouduan@clouduan-Lenovo-G480:~/code/openface/clouduan$ sudo ../demos/classifier.py infer ./generated-embeddings/classifier.pkl a.png 
/usr/local/lib/python2.7/dist-packages/sklearn/lda.py:4: DeprecationWarning: lda.LDA has been moved to discriminant_analysis.LinearDiscriminantAnalysis in 0.17 and will be removed in 0.19
  "in 0.17 and will be removed in 0.19", DeprecationWarning)

=== a.png ===
Predict person1 with 0.81 confidence.
```

# 2.web平台实时检测（Real-Time Web Demo）  

```
clouduan@clouduan-Lenovo-G480:~/code/openface/demos/web$ sudo ./start-servers.sh 

Starting the HTTP TLS server on port 8000
and the Secure WebSocket server on port 9000.

Access the demo through the HTTP server in your browser.
If you're running on the same computer outside of Docker, use https://localhost:8000
If you're running on the same computer with Docker, find the IP
address of the Docker container and use https://<docker-ip>:8000.
If you're running on a remote computer, find the IP address
and use https://<remote-ip>:8000.

WARNING: Chromium will warn on self-signed certificates. Please accept the certificate
and reload the app.

WebSocket Server: Logging to '/tmp/openface.websocket.log'

2017-07-04 23:44:16+0800 [-] Log opened.
2017-07-04 23:44:16+0800 [-] WebSocketServerFactory (TLS) starting on 9000
2017-07-04 23:44:16+0800 [-] Starting factory <autobahn.twisted.websocket.WebSocketServerFactory object at 0x7f512aae2950>
...
```
# 3.图片相似程度比较（Comparing two images）
```
clouduan@clouduan-Lenovo-G480:~/code/openface/demos$ sudo ./compare.py ../images/examples/{lennon*,clapton*}
Comparing ../images/examples/lennon-1.jpg with ../images/examples/lennon-2.jpg.
  + Squared l2 distance between representations: 0.782
Comparing ../images/examples/lennon-1.jpg with ../images/examples/clapton-1.jpg.
  + Squared l2 distance between representations: 1.059
Comparing ../images/examples/lennon-1.jpg with ../images/examples/clapton-2.jpg.
  + Squared l2 distance between representations: 1.170
Comparing ../images/examples/lennon-2.jpg with ../images/examples/clapton-1.jpg.
  + Squared l2 distance between representations: 1.402
Comparing ../images/examples/lennon-2.jpg with ../images/examples/clapton-2.jpg.
  + Squared l2 distance between representations: 1.552
Comparing ../images/examples/clapton-1.jpg with ../images/examples/clapton-2.jpg.
  + Squared l2 distance between representations: 0.379
```

# 4.实时嵌入可视化（Real-Time Face Embedding Visualization）

```
clouduan@clouduan-Lenovo-G480:~/code/openface/demos$ sudo ./sphere.py --networkModel nn4.small2.3d.v1.t7 
```
