---
title: python模块urllib.parse
date: 2017-07-27 11:13:24
tags:
- Python Syntax
- Web Crawler
- python module
categories:
- python语法笔记
---

官网说明

> This module defines a standard interface to break Uniform Resource Locator (URL) strings up in components (addressing scheme, network location, path etc.), to combine the components back into a URL string, and to convert a “relative URL” to an absolute URL given a “base URL.”

简单来说就是三个功能：拆分URL，构造URL，相对URL转换绝对URL

<!--more-->

### URL语法：

URL由三部分组成：资源类型、存放资源的主机域名、资源文件名。

URL的一般语法格式(带方括号[]的为可选项)：

```
protocol :// hostname[:port] / path / [;parameters][?query]#fragment
```

其中：

protocol指定使用的传输协议；

hostname（主机名）：有时，在主机名前也可以包含连接到服务器所需的用户名和密码（格式：username:password@hostname）；

port（端口号）：可选，省略时使用方案的默认端口，各种传输协议都有默认的端口号；

path（路径）：由零或多个“/”符号隔开的字符串，表示主机上的一个目录或文件地址；

query(查询)：可选，用于给动态网页传递参数，可有多个参数，用“&”符号隔开，每个参数的名和值用“=”符号隔开；

parameters（参数）：**我至今还没搞懂参数是怎么样的，好想不是在query里面传递的参数（那个参数解析不出来）**

fragment（信息片断）：字符串，用于指定网络资源中的片断。例如一个网页中有多个名词解释，可使用fragment直接定位到某一名词解释。

### urlparse函数

用法：urlparse(urlstring, scheme='', allow_fragments=True)

该函数把URL(scheme://netloc/path;parameters?query#fragment)拆分为6部分，并以元组的形式返回(scheme, netloc, path, parameters,query, fragment)。这个元组是支持索引访问，元组中的每个元素都是一个字符串也有可能是空字符串。但它其实是tuple的子类，叫做[namedtuple](https://docs.python.org/3/library/collections.html#collections.namedtuple)，具有自己独特的属性。注意%后面的部分不会被解析。

```python
>>> import urllib.parse
>>> o=urllib.parse.urlparse('https://docs.python.org/3/library/urllib.parse.html#url-parsing')
>>> o
ParseResult(scheme='https', netloc='docs.python.org', path='/3/library/urllib.parse.html', params='', query='', fragment='url-parsing')
>>> o.hostname
'docs.python.org'
>>> o.scheme
'https'
>>> o.geturl()
'https://docs.python.org/3/library/urllib.parse.html#url-parsing'
```

根据[RFC1808](https://tools.ietf.org/html/rfc1808.html)的语法规范，只有URL中有"//"时该函数才能解析出netloc，否则该函数会把输入的URL当做相对路径来解析，注意下面代码中的URL和netloc：

```python
>>> urllib.parse.urlparse('//google.com')
ParseResult(scheme='', netloc='google.com', path='', params='', query='', fragment='')
>>> urllib.parse.urlparse('google.com')
ParseResult(scheme='', netloc='', path='google.com', params='', query='', fragment='')
```

scheme参数给出了默认的寻址方案，只有URL中没有指定scheme时该参数才能使用（比如对于//google.com）：

```python
>>> o=urllib.parse.urlparse('//google.com',scheme='https')
>>> o
ParseResult(scheme='https', netloc='google.com', path='', params='', query='', fragment='')
>>> o.geturl()
'https://google.com'
```

如果allow_gragments值为False，fragment部分就不会被识别，会被解析为 path, parameters 或者 query component的一部分，而fragment部分会是一个空字符串，注意下面的“#url-parsing”片段：

```python
>>> o=urllib.parse.urlparse('https://docs.python.org/3/library/urllib.parse.html#url-parsing')
>>> o
ParseResult(scheme='https', netloc='docs.python.org', path='/3/library/urllib.parse.html', params='', query='', fragment='url-parsing')
>>> o=urllib.parse.urlparse('https://docs.python.org/3/library/urllib.parse.html#url-parsing',allow_fragments=False)
>>> o
ParseResult(scheme='https', netloc='docs.python.org', path='/3/library/urllib.parse.html#url-parsing', params='', query='', fragment='')
```

前面提到，该函数的返回值是一个[namedtuple](https://docs.python.org/3/library/collections.html#collections.namedtuple)，具有自己独特的属性，这些属性都是只读属性，见下表：

| 属性         | 索引值  | 值含义   | 默认值  |
| ---------- | ---- | ----- | ---- |
| scheme     | 0    | 协议    | 空字符串 |
| netloc     | 1    | 服务器地址 | 空字符串 |
| path       | 2    | 路径    | 空字符串 |
| parameters | 3    | 参数    | 空字符串 |
| query      | 4    | 查询部分  | 空字符串 |
| fragment   | 5    | 分片部分  | 空字符串 |
| username   |      | 用户名   | None |
| password   |      | 密码    | None |
| hostname   |      | 主机名   | None |

### urlsplit函数

用法：urlsplit(urlstring, scheme='', allow_fragments=True)

该函数和urlparse函数很像，只是不会解析URL中的参数（parameters）。因为根据[RFC2396](https://tools.ietf.org/html/rfc2396.html)，有一些URL的每个路径段（path segment）都带有参数，对于这样的URL就应该用此函数解析分割。因而该函数返回一个5元组(addressing scheme, network location, path, query, fragment identifier)。

同样的，返回的也是一个[namedtuple](https://docs.python.org/3/library/collections.html#collections.namedtuple)，属性较上表少了parameters。

### urljoin函数

该函数拼接组装URL，把baseURL与URL中的相对地址组装为一个绝对地址。

用法：urljoin(base, url, allow_fragments=True)

```python
>>>url=urlparse.urljoin('http://www.baidu.com','index.html')
>>> url
'http://www.baidu.com/index.html'
```

如果baseURL不以“/”结尾，那么baseURL最右边的部分会被相对URL中的换掉；如果以“/”结尾，那么相对URL会跟着baseURL后面：

```python
>>> urllib.parse.urljoin('http://www.clouduan.com/index.html','FAQ.html')
'http://www.clouduan.com/FAQ.html'
>>> urllib.parse.urljoin('http://www.clouduan.com/index.html/','FAQ.html')
'http://www.clouduan.com/index.html/FAQ.html'
```

如果相对URL有协议字段，则使用相对URL的协议：

```python
>>> urllib.parse.urljoin('http://www.baidu.com/faq.html','ftp://www.baidu.com/index.html')
'ftp://www.baidu.com/index.html'
```

对于上面的情况，当相对URL中不包含协议时，相对URL会被认为是一个路径进行处理：

```python
>>> urllib.parse.urljoin('http://www.baidu.com/faq.html','www.baidu.com/index.html')
'http://www.baidu.com/www.baidu.com/index.html'
```

如果baseURL和相对URL都有服务器地址且不相同的时候，则使用相对URL的服务器地址：

```
>>> urllib.parse.urljoin('http://www.baidu.com/index.html','http://www.github.com/index.html')
'http://www.github.com/index.html'
```

### urlunparse函数

利用urlparse()返回的值组合成一个URL

### urlunsplit函数

利用urlsplit()返回的值组合成一个URL



reference：

https://docs.python.org/3/library/urllib.parse.html

http://yucanghai.blog.51cto.com/5260262/1695439