---
title: LDA-线性判别分析
date: 2017-07-25 13:27:41
tags:
- Machine Learning
- Algorithm
categories:
- face recognition
---
判别分析(Discriminant Analysis)，是根据研究对象的各种特征值，判别其类型归属问题的一种对变量统计分析方法。
根据判别标准可以分为：距离判别、Fisher判别、Bayes判别法等。**在knn中用的是距离判别(而距离又有很多:欧式距离，马氏距离...)，朴素贝叶斯用的是Bayes判别、线性判别分析(LDA)用的是Fisher判别**
根据判别函数可以分为：线性判别、非线性判别。

<!--more-->
# 原理

+ 符号约定
  假设对于一个  空间有m个样本分别为：x1,x2,...xm,即每个x是一个n行的矩阵，其中ni表示属于i类的样本个数，假设有一个有C个类，则n1+n2+...+ni+nc=m 。    
  Sb .....................类间离散度矩阵       
  Sw .....................类内离散度矩阵   
  ni .....................属于i类的样本个数   
  xi .....................第i个样本   
  u  .....................所有样本的均值   
  ui .....................类i的样本均值   

+ 文字描述
现假设给定数据有两个类，要找到一个向量w，使得将数据投影到w上之后：
  1)类间距离(inter-class distance)较远：用投影后两个类的均值差度量。
  2)类内距离(intra-class distance)较近：用投影后每个类的方差度量。

+ 数学推导(以两类线性判别为例)
  映射函数(W^T*x实质上就是W和x的内积，w^T*x=|w|*|x|*cosA,当|w|=1时，此即为x在w上的投影，这也就是为何上文说“将数据投影到w上之后”)   
  {% asset_img lda1.png %}
  类别i的原始中心点(均值)为：（Di表示属于类别i的点)   
  {% asset_img lda2.png %}
  类别i投影后的中心点(均值,实质上等于mi的投影)：   
  {% asset_img lda3.png %}
  衡量类别i投影后，类别点之间的分散程度（方差）为：   
  {% asset_img lda4.png %}
  最终我们可以得到一个下面的公式，表示LDA投影到w后的损失函数：   
  {% asset_img lda5.png %}
  为方便计算w，损失函数可以化成下面的形式：   
  {% asset_img lda6.png %}
  我们希望分母越小，分子越大，所以要找w，使得J(w)最大，用拉格朗日乘子法求得：   
  {% asset_img lda7.png %}
  这样通过计算后，获得向量 W，生成新的 Y 轴，使两个类别的样本数据在新 Y 轴的投影能最大程度的分离。   
  找到向量w，即找到了映射函数，如果有一个新的样本，则计算   
  {% asset_img lda8.png %}
  大于u则属于第一类，小于u则属于第二类(u是所有样本的均值，就是下面*算法流程*中的w0，此处计算方法和下面*算法流程*中的原理一样)   

# 算法流程
+ 把来自两类w1，w2的训练样本集X分成与w1，w2分别对应的训练样本集X1，X2(已完成聚类，此时需要根据标签用LDA进行分类)
+ 计算各类样本的均值向量：m1，m2
+ 计算各类样本的内部离散度矩阵(方差):S1,S2
+ 计算总的类内离散度矩阵：Sw=S1+S2
+ 计算Sw的逆矩阵：Sw-1
+ 求解权向量：w=Sw-1(m1-m2)
+ 计算g(x):g(x)=w^T(x-1/2(m1+m2)) 此处w0=w^T 1/2(m1+m2)即为阀值，是所有样本投影后的均值。
+ 根据g(x)值判断测试向量的所属类别:
  如果 ../images/ml-algorithm/lda9.png , 则测试向量属于w1  
  如果 ../images/ml-algorithm/lda10.png, 则测试向量属于w2
  (**此处log的用意明白，但是P(w)的引入依据何在？**)

# 代码实现
  https://github.com/clouduan/practice/blob/master/ml-learning/lda.py
# LDA vs PCA
LDA和PCA很像，两者都对原始数据进行降维处理，最终的表现都是解一个矩阵特征值的问题，不同点如下：
+ LDA是supervised，PCA是unsupervised
  PCA没有分类标签，降维后需采用k-means或自组织映射网路等无监督的算法进行分类；LDA先对训练数据进行降维，然后找出一个线性判别函数。
+ 如图是一个2维的例子
  ../images/ml-algorithm/pca&lda.png  
  左边是PCA，它只是将整组数据整体映射到最方便表示这组数据的坐标轴上，但是映射时没有利用任何数据内部的分类信息(?)。因此，PCA后整组数据表示更加方便(降维并把信息损失降到最低)，但分类起来变得更加困难；右边的是LDA，两组数据映射到了另一个坐标轴上，变得更易区分(在低维就可区分，减少了很大的运算量)
+ PCA后的维度数目和数据维度相关，原始数据n维，PCA后为1、2～n维 
  LDA后的维度数目和类别个数相关，原始数据n维，一共有C个类别，那么LDA后维度为1、2～C-1维 
+ PCA投影的坐标系都是正交的(?)，LDA不一定。

附：
{% asset_img pca-lda.png lda和pca%}
{% asset_img ml-and-python.png 机器学习算法一览%}
# Reference
http://www.jianshu.com/p/e2306d5d632a 两类情形下的数学推导及代码实现   
http://www.jianshu.com/p/eadba35f1e68 多类情形下的数学推导(待研究)   
http://www.voidcn.com/blog/xietingcandice/article/p-2411740.html LDA和PCA，还有基础知识   
http://blog.csdn.net/sunmenggmail/article/details/8071502 好几种模式识别算法的说明   
http://blog.csdn.net/u012005313/article/details/50933517 数学推导及代码实现
