---
title: pycharm无法识别自定义模块的问题
date: 2017-08-14 23:27:58
tags:
- pycharm
---
在做qqbot时，写了一个自定义模块httpHandler专门处理网络请求，突然发现pycharm不能识别我的自定义模块：

```
Unresolved reference module 'httpHandler'
```
运行是没有问题的，但是无法识别就无法实现补全提示和代码跳转，而且底下总有一条红色的波浪线标记看起来也蛮蛋疼的。
<!--more-->

网上有人说“自定义模块要放在一个 package 中，也就是文件夹中必须有一个\__init__.py 才能作为自定义包来引用”，可是怎样添加init文件呢？

好在pycharm本身可以很方便地解决这个问题，project 作为 Pycharm Project 的根目录的话，是完全没有问题的，因此只需要选中目录文件夹，然后右键：Mark Directory As Source Root.

这样就可以识别了，而且会在目录下新建一个\__pycache__的文件，里面存放这被引用模块的二进制文件pyc。
