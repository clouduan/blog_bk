---
title: NLP-获取文本语料和词汇资源  
date: 2017-07-25 13:03:50
tags: 
- NLP
- python
categories:
- 《python自然语言处理》学习笔记
---
from nltk.corpus import sample  
corpus模块包含很多不同的语料库 
<!--more--> 
## 1.文本语料库的结构
{% asset_img struc_of_corpus.PNG 文本语料库的结构 %}

最简单的一种没有任何结构，仅是一个文本集合；
一些语料库可能会按照（主题，文体等）组织分类；
一些语料库的分类可能会重叠；
另外一些表示随时间变化语言用法的变化。

## 2.基本语料库操作函数  
| 函数                               |         功能          |
| -------------------------------- | :-----------------: |
| corpus.fileids()                 |  返回列表，列表包含语料库中的文件名  |
| corpus.fileids([categories])     | 返回[categories]对应的文件 |
| corpus.categories()              |  返回列表，列表包含语料库中的分类   |
| corpus.categories([fileids])     |  返回[fileids]对应的分类   |
| corpus.raw()                     |   语料库的原始内容，字符串形式    |
| corpus.raw(fileids=[f1,f2,f3])   |      指定文件的原始内容      |
| corpus.raw(categories=[c1,c2])   |      指定分类的原始内容      |
| corpus.words()                   |    语料库中的词汇，列表形式     |
| corpus.words(fileids=[f1,f2,f3]) |       指定文件的词汇       |
| corpus.words(categories=[c1,c2]) |       指定分类的词汇       |
| corpus.sents()                   |    语料库中的句子，列表形式     |
| corpus.sents(fileids=[f1,f2,f3]) |       指定文件的句子       |
| corpus.sents(categories=[c1,c2]) |       指定分类的句子       |

## 3.载入自己的语料库    
>PlaintextCorpusReader()  
>BracketParseCorpusReader()   

## 4.条件频率分布      
基本形式：(条件，事件)  
**条件频率分布**是**频率分布**的集合，每个频率分布有不同的条件：条件通常是不同的**文本类型**，事件是某些词的**频数**    

常用函数：  

| 函数                               |        功能         |
| -------------------------------- | :---------------: |
| cfd=ConditionalFreqDist(pairs)   |  从配对链表中创建条件频率分布   |
| cfd.conditions()                 |     条件频率分布的条件     |
| cfd[condition]                   | 此条件下的频率分布，此时即频率分布 |
| cfd.tabulate()                   |        绘表         |
| cfd.tabulate(samples,conditions) |    绘表，指定样本和条件     |
| cfd.plot()                       |        绘图         |
| cfd.plot(samples,conditions)     |                   |

## 5.其它语料库资源  
1.**词汇列表语料库**  

words: 词汇语料库，仅仅包含词汇列表，用于UNIX拼写检查    
stopwords: 停用词语料库，包括的是高频词汇如a,the...通常可以将其从文档中过滤  
names: 名字语料库  
`print(nltk.corpus.names.words())`  
2.**发音字典**   

采取表格词典形式：每一行含有一个词及其一些性质   
`print(nltk.corpus.cmudict.entries())` #entries 即为“条目”之意   
3.**比较词表**  

采取表格词典形式   
`from nltk.corpus import swadesh`    
`print(swadesh.fileids())`    
`print(swadesh.entries(['en','fr']))`  

## 6.wordnet   
难于理解  


**附：[代码练习](https://github.com/clouduan/practice/blob/master/nlp-learning/ex2.py)**
