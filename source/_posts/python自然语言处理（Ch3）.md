---
title: NlP-处理原始文本
date: 2017-07-25 13:05:13
tags:
- python
- NLP
categories:
- 《python自然语言处理》学习笔记
---
## 1.从网络和硬盘访问文本  
<!--more-->
包括电子书，HTML，RSS，本地文件等，处理程序为：  
> 1.先读取为原生文本字符串（raw string）  
> 2.然后分词，转换为list   
> `tokens=nltk.word_tokenize(raw)     #type 'list'`   
> 3.创建nltk文本   
> `text=nltk.Text(tokens)             #type 'nltk.text.Text'`   

{% asset_img trans_to_nltk.Text_object.png 处理流程%}
## 2.字符串的操作  

## 3.通过Unicode进行文字处理      

## 4.使用正则检测词组搭配   
手机九键输入法的提示原理  
{% asset_img T9_IM.PNG 九键%}
事件构建好单词列表`wordlist`，如果用户输入序列  4653，则其匹配的词可以由下面列表得到：  
`[w for w in wordlist if re.search('^[ghi][mno][jlk][def]$',w)]`   
## 5.正则的有益应用    
>1.提取字符块  
>`re.findall()`  
>2.查找词干   
>去前后缀  
## 6.规范化文本  
1.词干提取器  
>nltk包括了现成的提取器，如Porter和Lancaster  
>`porter=nltk.PorterStemmer()`  
>`lancaster=nltk.LancasterStemmer()`  
>`[porter.stem(t) for t in tokens] #tokens是一个word的list`  
>`[lancaster.stem(t) for t in tokens]`  
>2.词形归并  
>WordNet词形归并器删除词缀产生词，它处理的词必须在其字典中。这就使得其速度比较慢      
>`wnl=nltk.WordNetLemmatizer()`  
>`[wnl.lemmatize(t) for t in tokens]`  
## 7.正则为文本分词  
>1.简单的分词  
>`re.split()`  
>`re.findall()`  
>{% asset_img regexp.PNG 正则符号%}
>2.nltk的正则分词器  
>`nltk.regexp_tokenize(text,pattern)`  
## 8.分割  
>1.断句  
>nltk的句子分割器  
>`sent_tokenizer=nltk.data.load('tokenizers/punkt/english.pickle')`  
>`sents=sent_tokenizer.tokenize(text)  #text为一长串字符串`  
>`pprint.pprint(sents)`  
>2.分词  
>原理：给字符串配套一个字位串，每个对应的字符都标注了一个布尔值来指示这个字符后面是否有一个分词标志。  
>此处**目标函数**法和**模拟退火算法的非确定搜索**很重要，在[代码练习3](https://github.com/clouduan/practice/blob/master/NLP/ex3_3.py)中再详细说明**  
## 9.格式化输出  
“%”  

附：
**[代码练习1](https://github.com/clouduan/practice/blob/master/nlp-learning/ex3_1.py)**
**[代码练习2](https://github.com/clouduan/practice/blob/master/nlp-learning/ex3_2.py)**
**[代码练习3](https://github.com/clouduan/practice/blob/master/nlp-learning/ex3_3.py)**

