---
title: 图像识别中的卷积神经网络
date: 2017-07-25 13:24:34
tags:
- face recognition
- CV
- Machine Learning
- Algorithm
- CNN
categories:
- face recognition
---
## 图像基本知识  

一个图像可以用一个矩阵A(应该说是方阵)来表示它，A的每一个元素是一个rgb值(即f(x,y)=RGB-value)。 有了这个图像<->矩阵的转换关系后，我们就可以用线性代数的知识对这个图像做处理。

<!--more-->

现假设我们有一个图像，尺寸是32x32x3，代表宽32像素，高32像素，3个颜色通道，即图片在计算机内是一个三维数组，显现在屏幕上就是32x32的矩阵其中每个元素是一个包含三个数值的数组。 

## 卷积

连续空间的一维卷积定义： f(x)与g(x)的卷积是 f(t-x)g(x) 在t从负无穷到正无穷的积分值.t-x要在f(x)定义域内,所以看上去很大的积分实际上还是在一定范围的.
实际的过程就是f(x) 先做一个Y轴的反转,然后再沿X轴平移t就是f(t-x),然后再把g(x)拿来,两者乘积的值再积分.

离散空间：把积分符号换成求和就是了.

卷积常见的应用：1. 微分方程，2. 傅立叶变换及其应用，3. 概率论，4. 卷积神经网；

信号处理时，输出函数是输入函数和系统函数（在信号处理中称为“滤波”）的卷积；在图像处理时，两组幅分辨率不同的图卷积之后得到的互相平滑的图像可以方便处理。

## 图像处理中的卷积 

图像处理中用到的大多是二维卷积的离散形式

图像f,模板g,然后将模版g在图像中移动,每到一个位置,就把f与g的定义域相交的元素进行乘积并且求和,得出新的图像一点,就是被卷积后的图像.此处的模板称为卷积核，卷积核沿反对角线翻转之后才与图像作卷积。 ![卷积反转](./images/反转卷积核.png)   

举一个最简单的均值滤波的例子：  
![图像卷积](./images/卷积均值滤波.png)     

上面是一个 3x3 的均值滤波核，也就是卷积核，下面是被卷积图像，这里简化为一个二维 5x5 矩阵。当卷积核运动到图像右下角处（卷积中心和图像对应图像第 4 行第 4 列）时，它和图像卷积的结果如下图所示：  
![二维卷积示例](./images/2d-convolution.png)   

由此可见，二维卷积在图像中的效果就是：对图像的每个像素的邻域（邻域大小就是核的大小）加权求和得到该像素点的输出值。滤波器核在这里是作为一个“权重表”来使用的。


## 常规神经网络  
+ 神经元  
  这种能自动对输入的东西(是一个数值向量，叫做特征向量)进行分类的机器，就叫做分类器。  
  一个点可以把一条直线一分为二，一条直线可以把平面一分为二，一个平面可以把三维空间一分为二，一个n-1维超平面可以把n维空间一分为二，分开的两边属于不同分类，这样的分类器叫做神经元。而平面的方程是 ax+by+c=0,若对(x0,y0),ax0+by0+c>0,或<0,则可以判定该点所属类别,因此这条直线模型就是神经元。现将其推广到n维，n维超平面的方程为 S=a1x1+a2x2+...+anxn+a0=0。   
  为什么叫它神经元呢？因为它和生命体神经元的工作原理相似：从多个感受器接受电信号 x1,x2,...,xn,然后进行处理(a1x1+a2x2...实质上就是加权再相加),最后得出判断(S)并发出电信号。   
  神经网络就是一个有向图，神经元就是节点，负责进行运算并做出判断,实质上就是做决策的。 神经元的训练就是先固定分类器，然后拿已有的认为标记的数据进行一个一个试错，让分类器不断改变自身位置，最终训练到正确区分的位置。   
+ 神经网络   
  上述神经元称为MP神经元，它有两个缺点：   
  1)它把空间的一侧变为0，另一侧变为1，这个不可微，不利用数学分析。因此用一个和0-1 阶跃函数类似但是更平滑的Sigmoid函数代替，这样神经网络的训练就可以用**梯度下降法**来构造了，这就是**反向传播算法**。(?)   
  2)上面提到的线性神经元，只能一次分两个类，如果多个类就无余力了，解决方法是构造多层神经元，构成神经网络，一个神经元的输出是另一个的输入，一层层级联起来，把不同层的分类结果再进行交并(异或)运算，即得到最终的结果。   
  3)线性的分类针对线性可分的数据可以很方便地分开，但假设要用一条直线分开下面的两组数据，就需要对空间进行变换 
  ![空间变换](./images/空间变换.png)      
  对二维空间进行扭曲(扭曲的方法见下面*神经网络中的空间变换*)，变成三维空间，然后找到一个三维空间的平面进行分割。这点非常像SVM的处理思路，这儿有个短视频https://www.youtube.com/watch?v=3liCbRZPrZA。   

+ 神经网络的空间变换   
  y=a(Wx+b):x是输入向量，y是输出向量，b是偏移向量，W是权重矩阵，a()是激活函数。其对输入空间的操作大体分为三种：变维放缩旋转(由W实现)，平移(由b实现)，弯曲(由a()实现)。   

+ 神经网络的训练   
  神经网络的训练依靠反向传播算法(BP)：最开始输入层输入特征向量，网络层层计算获得输出，输出层发现输出和正确的类号不一样(损失函数或目标函数(loss function or objective function)用于衡量预测值和目标值的差异。loss function的输出值（loss）越高表示差异性越大。那神经网络的训练就变成了尽可能的缩小loss的过程)，这时它就让最后一层神经元进行参数调整，最后一层神经元不仅自己调整参数，还会勒令连接它的倒数第二层神经元调整，层层往回退着调整。经过调整的网络会在样本上继续测试，如果输出还是分错，继续来一轮回退调整，直到网络输出满意为止。   
  1、利用初始权量，在输入向量上运行前向传播，从而得到所有网络所有神经元的输出。    
  2、这样，每个输出层神经元都会得到一个误差，即输出值与实际值之差。    
  3、计算作为神经元权量的函数的误差的梯度，然后根据误差降低最快的方向调整权量。    
  4、将这些输出误差反向传播给隐藏层以便计算相应误差。    
  5、计算这些误差的梯度，并利用同样的方式调整隐藏层的权量。    
  6、不断迭代，直到网络收敛。   

## 图像处理中的卷积神经网络   
**结构**：INPUT -> [[CONV -> RELU]*N -> POOL?]*M -> [FC -> RELU]*K -> FC   
**过程**：输入一个多维数组（图像在计算机中的存在形式），然后通过卷积层对其做卷积（这个过程就是滤波器filter对图像数据做內积），最后输出新的二维数组。这个过程实质上就是做降维运算，将原本及其复杂的数据进行压缩。   

![nn&cnn](https://pic2.zhimg.com/2ef08bb4cf60805d726b2d6db39dd985_b.jpg)

卷积层是核心层，卷积层中，神经元与输入层中的一个局部区域相连（这一点很重要，“局部连接”，这是和常规神经网络的极大不同），每个神经元都计算自己与输入层相连的小区域与自己权重的内积。卷积层会计算所有神经元的输出。    

上面说到的滤波器filter，也叫滤波矩阵，卷积核，是一组带着固定权重的神经元。 

![局部连接和全连接](http://www.36dsj.com/wp-content/uploads/2015/03/511-600x224.jpg)






http://lib.csdn.net/article/deeplearning/42986 很好的一篇文章。

http://mengqi92.github.io/ 关于机器学习，线代理解，图像处理
