---
title: pth文件的编码(2)
date: 2017-06-04 10:36:50
tags: python 编码
---
# 针对py2
<p><strong>【所谓unicode】</strong></p>
<p>unicode是一种类似于符号集的抽象编码，它只规定了符号的二进制代码，却没有规定这个二进制代码应该如何存储。也就是它只是一种内部表示，不能直接保存。所以存储时需要规定一种存储形式，比如utf-8和utf-16等。理论上unicode是一种能够容纳全世界所有语言文字的编码方案。（其他编码格式不再多说）</p>
<p><strong>【所谓GB码】</strong></p>
<p>GB就是“国标”的意思，即：中华人民共和国国家标准。GB码是面向汉字的编码，包括GB2312（GB2312-80），GBK，GB18030，表示范围从小到大递增，而且基本是向下兼容的。此外经常遇到一种叫做CP936的编码，实际上可以大概看做是GBK。</p>
<p><strong>【判断编码】</strong></p>
<p>1、使用isinstance(s, str)来判断一个字符串是否为一般字符串（str为ascii类型的字符串，utf-8、utf-16、GB2312、GBK等都是ascii类型的字符串）；</p>
<p>使用isinstance(s, unicode)来判断一个字符串是否为unicode编码形式的字符串（unicode编码的字符串才是unicode类型的字符串）。</p>
<p>2、使用type()或者.__class__</p>
<p>在编码正确的情况下：</p>
<p>例如：stra = "中", 则使用type(stra)的结果是&lt;type 'str'&gt;，表明为ascii类型字符串；</p>
<p>例如：strb = u"中", 则使用type(strb)的结果是&lt;type 'unicode'&gt;，表明为unicode类型字符串。</p>
<div class="jb51code">
<pre class="brush:py;">
>>>tmp_str = 'tmp_str'
>>>print tmp_str.__class__   #&lt;type 'str'&gt;
>>>print type(tmp_str)    #&lt;type 'str'&gt;
>>>print type(tmp_str).__name__ #str
>>>tmp_str = u'tmp_str'
>>>print tmp_str.__class__   #&lt;type 'unicode'&gt;
>>>print type(tmp_str)    #&lt;type 'unicode'&gt;
>>>print type(tmp_str).__name__ #unicode
</pre>
</div>
<p>3、最好的办法是使用chardet判断，特别是在web相关的操作中，例如抓取html页面内容时，页面的charset标签只是标示编码，有时候不对，而且页面内容中一些中文可能超出了标示编码的范围，此时用charset检测最为方便准确。</p>
<p>（1）安装办法：下载chardet后，将解压得到的chardet文件夹放在Python安装目录的\Lib\site-packages目录下，在程序中使用import chardet即可。</p>
<p>（2）使用办法1：检测全部内容判断编码</p>
<div class="jb51code">
<pre class="brush:py;">
>>>import urllib2
>>>import chardet
>>>res = urllib2.urlopen('http://www.jb51.net')
>>>res_cont = res.read()
>>>res.close()
>>>print chardet.detect(res_cont) #{'confidence': 0.99, 'encoding': 'utf-8'}
</pre>
</div>
<p>detect函数返回值为一个包含2个键值对的字典，第一个是检测置信度，第二个就是检测到的编码形式。</p>
<p>（3）使用办法2：检测部分内容判断编码，提高速度</p>

<pre class="brush:py;">
import urllib2
from chardet.universaldetector import UniversalDetector
res = urllib2.urlopen('http://www.jb51.net')
detector = UniversalDetector()
for line in res.readlines():
 #detect untill reach threshold
 detector.feed(line)
 if detector.done:
     break
detector.close()
res.close()
print detector.result
{'confidence': 0.99, 'encoding': 'utf-8'}
</pre>
</div>
<p><strong>【转换编码】</strong></p>
<p>1、从具体的编码（ISO-8859-1[ASCII码]，utf-8，utf-16，GBK，GB2312等）转换为unicode，直接使用unicode(s, charset)或者s.decode(charset)，其中charset为s的编码（注意unicode在使用decode()时会出错）；</p>
<pre>
>>>#将任意字符串转换为unicode
>>>def to_unicode(s, encoding):
>>> if isinstance(s, unicode):
>>>  return s
>>> else:
>>>  return unicode(s, encoding)
</pre>
</div>
<p>注意：这里在decode()的时候，如果遇到非法字符（比如不标准的全角空格\xa3\xa0，或者\xa4\x57，真正的全角空格是\xa1\xa1），就会报错。</p>
<p>解决办法：采用'ignore'模式，即：stra.decode('...', 'ignore').encode('utf-8')。</p>
<p>解释：decode的函数原型是decode([encoding],[errors='strict'])，可以用第二个参数控制错误处理的策略。</p>
<p>默认的参数就是strict，代表遇到非法字符时抛出异常；如果设置为ignore，则会忽略非法字符；如果设置为replace，则会用&#63;取代非法字符；如果设置为xmlcharrefreplace，则使用XML的字符引用。</p>
<p>2、从unicode转换为具体的编码，也是直接用s.encode(charset)，其中s为unicode编码，charset为具体的编码（注意非unicode在使用encode()时会出错）；</p>
<p>3、自然地，从一种具体编码转换为另一种具体编码，就可以先decode成unicode再encode成最终编码了。</p>
<p><strong>【python命令行编码（系统编码）】</strong></p>
<p>用python自带的locale模块来检测命令行的默认编码（也就是系统的编码）和设置命令行编码：</p>
<div class="jb51code">
`import locale`
`#get coding type`
`print locale.getdefaultlocale() #('zh_CN', 'cp936')`
`#set coding type`
`locale.setlocale(locale.LC_ALL, locale='zh_CN.GB2312')`
`print locale.getlocale() #('zh_CN', 'gb2312')`
</div>
<p>表明当前系统的内部编码是cp936，近似于GBK。实际上中文XP和WIN7的系统内部编码都是cp936（GBK）。</p>
<p><strong>【python代码中的编码】</strong></p>
<p>1、python代码中的字符串在未被指定编码的情况下，默认编码与代码文件本身的编码一致。举个例子：str = '中文'这个字符串，如果是在utf8编码的代码文件中，该字符串就是utf8编码；如果是在gb2312的文件中，该字符串就是gb2312编码。那么代码文件本身的编码怎么知道呢？</p>
<p>（1）自己指定代码文件的编码：在代码文件的头部加上“#-*- coding:utf-8 -*-”来声明该代码文件为utf-8编码。此时未被指定编码的字符串的编码都变成了utf-8。</p>
<p>（2）在没有指定代码文件的编码时，创建代码文件时使用的是python默认采用的编码（一般来说是ascii码，在windows中实际保存为cp936（GBK）编码）。通过sys.getdefaultencoding()和sys.setdefaultencoding('...')来获取和设置该默认编码。</p>
<div class="jb51code">
<pre class="brush:py;">
import sys
reload(sys)
print sys.getdefaultencoding() #ascii
sys.setdefaultencoding('utf-8')
print sys.getdefaultencoding() #utf-8

</pre>
</div>
<p>结合（1）和（2）做个试验：指定代码文件编码为utf-8时，用notepad++打开显示的是utf-8无DOM编码；未指定代码文件编码时，用notepad++打开显示的是ANSI编码（压缩编码，默认的保存编码形式）。</p>
<p><img alt="" src="http://files.jb51.net/file_images/article/201607/201671102207574.jpg&#63;201661102850" /></p>
<p>（3）如何永久地将python默认采用的编码设置为utf-8呢？有2种方法：</p>
<p>第一个方法&lt;不推荐&gt;：编辑site.py，修改setencoding()函数，强制设置为 utf-8；</p>
<p>第二个方法&lt;推荐&gt;：增加一个名为 sitecustomize.py的文件，存放在安装目录下的\Lib\site-packages目录下</p>
<p>sitecustomize.py是在site.py被import执行的，因为 sys.setdefaultencoding()是在site.py的结尾处被删除的，所以可以在 sitecustomize.py使用 sys.setdefaultencoding()。</p>
<p>2、python代码中的字符串如果被指定了编码，举个例子：str = u'中文'，该字符串的编码被指定为unicode（即python的内部编码）。</p>
<p>（1）这里有个误区需要注意！假如在py文件中有如下代码：</p>
<div class="jb51code">
<pre class="brush:py;">
stra = u"中"
print stra.encode("gbk")
</pre>
</div>
<p>按上面说的stra是unicode形式，直接encode称gbk编码应该没问题啊？但是实际执行时会报错“UnicodeEncodeError: 'gbk' codec can't encode character u'\xd6' in position 0: illegal multibyte sequence”。</p>
<p>原因在于：python解释器在导入python代码文件并执行时，会先查看文件头有没有编码声明（例如#coding:gbk等）。如果发现声明，会将文件中的字符串都先解释成unicode的形式（这里先用默认编码gbk（cp936）将stra解码成unicode编码'd6d0'后保存），之后执行stra.encode('gbk')时，由于stra已经是unicode编码且'd6d0'在gbk的编码范围内，所以编码不会出现错误；如果文件头没有编码声明，则不会进行上述过程中的解码操作（这里就直接使用stra的unicode编码'd6'），之后执行stra.encode('gbk')时，由于'd6'不在gbk的编码范围所以报错。</p>
<p>（2）为避免这种类型的错误，最好在代码文件头上声明编码，或者麻烦点每次使用setdefaultencoding()。</p>
<p>（3）总的来说就是unicode是python解释器的内码，所有代码文件在导入并执行时，python解释器会先将字符串使用你指定的编码形式解码成unicode，然后再进行各种操作。所以不管是对字符串的操作，还是正则表达式，还是读写文件等等最好都通过unicode来进行。</p>
<p><strong>【python中其他编码】</strong></p>
<p>文件系统的编码：sys.getfilesystemencoding()<br />
终端的输入编码：sys.stdin.encoding<br />
终端的输出编码：sys.stdout.encoding</p>


【转自】http://www.jb51.net/article/87739.htm，写得太好了　　
P.S.      [python 编码大全](http://www.jb51.net/Special/788.htm)